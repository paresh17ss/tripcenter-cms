module.exports = app => {
  const controller = require("../controller/supplier_demo.controller");
  app.post("/newSupplierDemo", controller.newSupplierDemo);
};
