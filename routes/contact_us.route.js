module.exports = app => {
  const controller = require("../controller/contact_us.controller");
  app.post("/add-ContactUS", controller.addContactUs);
  app.get("/getContactUsList", controller.getContactUsList);
  app.post('/deleteContactUS',controller.deleteContactUs);
  app.post('/editContactUS',controller.updateContactUs);
};
