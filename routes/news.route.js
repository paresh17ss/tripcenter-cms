module.exports =(app)=>{
    const controller = require('../controller/news.controller')
    
    app.post('/addnews',controller.addnews);
    app.get('/newslist',controller.getnewslist);
    app.post('/getnews',controller.getnews);
    app.post('/deletenews',controller.deletenews);
    app.post('/updatenews',controller.updatenews);

    app.post("/frontend-news-list", controller.frontendnewslist);
    app.post("/frontend-news-detail", controller.frontendnewsdetail);
    app.post("/get-latest-news", controller.GetLatestNewsList);
    app.post("/get-popular-news", controller.GetPopularNewsList);
}