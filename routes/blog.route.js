module.exports = app => {
  const controller = require("../controller/blog.controller");
  app.post("/addblog", controller.addblog);
  app.get("/bloglist", controller.getbloglist);
  app.post("/getblog", controller.getblog);
  app.post("/deleteblog", controller.deleteblog);
  app.post("/updateblog", controller.updateblog);
  app.post("/frontend-blog-list", controller.frontendbloglist);
  app.post("/frontend-blog-detail", controller.frontendblogdetail);
  app.post("/get-latest-blog", controller.GetLatestBlogList);
  app.post("/get-popular-blog", controller.GetPopularBlogList);
};
