module.exports = (app)=>{
     const controller  = require('../controller/roles.controller');
     app.get('/rolelist',controller.getrolelist),
     app.post('/addrole',controller.addrole),
     app.post('/editrole',controller.updaterole);
}