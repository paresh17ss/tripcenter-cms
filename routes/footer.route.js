module.exports =(app)=>{
    const controller = require('../controller/footer.controller')
    
    app.get('/footerlist',controller.getfooterlist);
    app.post('/addfooter',controller.addfooter);
    app.post('/deletefooter',controller.deletefooter);
    app.post('/getfooter',controller.getfooter);
    app.post('/editfooter',controller.updatefooter);
   
}