module.exports = (app)=>{
     const controller  = require('../controller/categories.controller');
     
     app.get('/categorylist',controller.getcategorylist),
     app.post('/addcategorie',controller.addcategory),
     app.post('/deletecategory',controller.deletecategory);
     app.post('/editcategory',controller.updatecategory);
    
     
}