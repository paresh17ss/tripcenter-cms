module.exports = app => {
  const controller = require("../controller/news_comment.controller");
  app.post("/add-news-comment", controller.addComment);
  app.post("/delete-news-comment", controller.deleteComment);
  app.post("/get-comment-by-news-id", controller.getCommentByNewsId);
  app.get("/getnewscommentlist", controller.getNewsCommentList);
  app.post("/get-reply-by-news-comment", controller.getCommentReplyByCommentId);
  app.put("/update-news-comment", controller.updateComment);
};
