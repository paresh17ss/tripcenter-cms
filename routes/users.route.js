module.exports =(app)=>{
    const controller = require('../controller/users.controller')
    
    app.post('/adduser', controller.adduser);
    app.get('/userlisting', controller.getuserlist);
    app.post('/deleteuser', controller.deleteuser);
    app.post('/edituser', controller.updateuser);
    app.post('/loginUser', controller.loginUser);
    app.post('/logoutUser', controller.logoutUser);
}