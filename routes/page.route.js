module.exports = (app) =>{
	const controller = require('../controller/page.controller');
	app.post('/pagelist',controller.getpagelist);
	app.post('/add',controller.addpage);
	app.post('/uplodimg',controller.uplodimg);
	app.post('/deletepage',controller.deletepage);
	app.post('/updatepage',controller.updatepage);
	app.post('/get-html',controller.getHtml);	
	app.post('/edit',controller.getpage);
	app.get('/get-master-html', controller.getHomeHtml);
}