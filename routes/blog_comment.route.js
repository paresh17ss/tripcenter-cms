module.exports = app => {
  const controller = require("../controller/blog_comment.controller");
  app.post("/add-blog-comment", controller.addComment);
  app.post("/delete-comment", controller.deleteComment);
  app.post("/get-comment-by-blog-id", controller.getCommentByBlogId);
  app.get("/getcommentlist", controller.getCommentList);
  app.post("/get-reply-by-comment", controller.getCommentReplyByCommentId);
  app.put("/update-comment", controller.updateComment);
};
