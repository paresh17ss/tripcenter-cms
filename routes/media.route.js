module.exports = (app) =>{
	const controller = require('../controller/media.controller');
	
	app.post('/addmedia',controller.addmedia);	
	app.get('/medialist',controller.getmedialist);
	app.post('/editmedia',controller.updatemedia);
	app.post('/deletemedia',controller.deletemedia);
}