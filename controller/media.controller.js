const models = require("../models");
var fs = require("fs");
var azure = require("azure-storage");
var blobService = azure.createBlobService(
  "tripcenterindia",
  "c84GhN5H5hNI7JbHYu+9KkcMmG3jOqV9PqnLw7vir9ceetEyNWYlejgf/BOKDgZ28tVkwGNvn/yMfNlxvOSU2w=="
);
const images = require("../controller/common.controller");

//Get list method for media
exports.getmedialist = (req, res) => {
  models.media
    .findAll({ where: { isDeleted: 0 } })
    .then(data => {
      res.json({ status: true, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

// add method for media
exports.addmedia = (req, res) => {
  var title = req.body.title;

  models.media.findOne({ where: { title: title, isDeleted: 0 } }).then(async data => {
    if (data == null) {
      let sampleFile = req.files.file;
      let type = sampleFile.mimetype.split("/")[1];
      let fileName = new Date().getTime();
      // image file upload local and severside
      let image = await images.upload(
        sampleFile,
        "./src/assets/images/" + fileName + "." + type,
        true,
        "./media/" + fileName + "." + type
      );
      if (image.status == true) {
        models.media
          .create({
            title: req.body.title,
            image: "media/" + fileName + "." + type
          })
          .then(function(data) {
            res.json({ status: 200, message: "success", data: data });
          })
          .catch(error => {
            res.json({ status: false, message: error });
          });
      } else {
        res.json({ status: false, message: "Something went wrong" });
      }
    } else {
      res.json({ status: false, message: "Title already exists!" });
    }
  });
};

// update method for media
exports.updatemedia = async (req, res) => {
  title = req.body.title;
  models.media
    .findOne({ where: { title: title, isDeleted: 0, id: { $ne: req.body.id } } })
    .then(async data => {
      if (data == null) {
        if (req.files != null) {
          let sampleFile = req.files.file;
          let type = sampleFile.mimetype.split("/")[1];
          let fileName = new Date().getTime();
          // upload media to only server
          let image = await images.upload(
            sampleFile,
            "./src/assets/images/" + fileName + "." + type,
            false,
            "./media/" + fileName + "." + type
          );
          if (image.status == true) {
            updateValues = {
              title: title,
              image: "media/" + fileName + "." + type
            };
            update();
          } else {
            res.json({ status: false, message: "Something went wrong" });
          }
        } else {
          updateValues = { title: title };
          update();
        }

        function update() {
          models.media
            .update(updateValues, { where: { id: req.body.id } })
            .then(result => {
              if (result) {
                res.json({ status: 200, message: "success" });
              } else {
                res.json({ status: false, message: "Something went wrong" });
              }
            });
        }
      } else {
        res.json({ status: 301, message: "Title already exists!" });
      }
    });
};

// delete method for media
exports.deletemedia = (req, res) => {
  const id = req.body.id;
  updateValues = { isDeleted: 1 };
  models.media
    .update(updateValues, { where: { id: id } })
    .then(data => {
      res.json({ status: 200, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};