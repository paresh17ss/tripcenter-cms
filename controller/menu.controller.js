const models = require('../models');

// Get method for menu
exports.menu = async(req, res) => {
	models.menu.findAll({include:[{model:models.permission}]}).then((data) => {
		let menu = data;
		res.json({status:true,data:menu})
		
	}).catch((error) => {
		res.json({ status: false, message: error })
	});
}

