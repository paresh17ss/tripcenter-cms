const models = require('../models');

// Get List method for roles
exports.getrolelist = (req,res)=>{
    models.roles.findAll().then((data)=>{
            res.json({status:true,message:"success",data:data})
    }).catch((error)=>{
        res.json({status:false,message:error});
    })
}

// Add method for role
exports.addrole = (req,res) =>{
    let key = req.body.key;
    let displayValue = req.body.displayValue;
    let isSuperUser = req.body.isSuperUser;
    models.roles.findOne({where:{key:key}}).then(data=>{
        if(data == null){
            models.roles.create({key:key,displayValue:displayValue, isSuperUser: isSuperUser}).then((data)=>{
                res.json({ status: 200, message: 'success', data: data })
            }).catch((error)=>{
                res.json({ status: false, message: 'something went wrong', error})
            })
        }else{
            res.json({ status: 301, message: "Role already exists!" });
        }
    })
}

// Update method for role
exports.updaterole  = (req,res) => {
  let id = req.body.id;
  let key = req.body.key;
  let displayValue = req.body.displayValue;
  let isSuperUser = req.body.isSuperUser;
    models.roles.findOne({where:{ key:key, id:{$ne: id}}}).then(data=>{
        if(data == null){
            updateValues = { key: key, displayValue: displayValue, isSuperUser: isSuperUser };
            models.roles.findOne({ where: {  id:id } }).then((data) => {
                if(data){
                    models.roles.update(updateValues,{ where: { id: id } }).then((result) => {
                        if (result) {
                            res.json({ status: 200, message: "success", data: data });
                        } else {
                            res.json({ status: false, message: "Something went wrong" });
                        }
                    });
                }
            }).catch((error) => {
                res.json({ status: false, message: error })
            });
        } else {
            res.json({ status: 301, message: "Role already exists!" });
        }
    });
};