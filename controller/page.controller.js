const models = require('../models');
const request = require('request');
const fs = require('fs');
const cheerio = require('cheerio');
const fileUpload = require('express-fileupload');
var azure = require('azure-storage');
var blobService = azure.createBlobService(
  'tripcenterindia',
  'c84GhN5H5hNI7JbHYu+9KkcMmG3jOqV9PqnLw7vir9ceetEyNWYlejgf/BOKDgZ28tVkwGNvn/yMfNlxvOSU2w=='
);
const datatable = require(`sequelize-datatables`);
const images = require('../controller/common.controller');

// Get list method for page
exports.getpagelist = (req, res) => {
  models.page
    .findAll({ where: { isDeleted: 0, template: req.body.templateFilter } })
    .then(data => {
      res.json({ status: 200, message: 'success', data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

//Get method for page
exports.getHtml = (req, res) => {
  models.page
    .findOne({ where: { url: req.body.url, isDeleted: 0 } })
    .then(project => {
      if (project != null) {
        const $ = cheerio.load(project.publish_html);
        $('a').each(function() {
          let href = $(this).attr('href');
          let url = '';
          if (href == '#' || href == 'javascript:void(0)') {
          } else {
            $(this).attr('(click)', `router('${href}')`);
            $(this).attr('href', 'javascript:void(0)');
          }
        });
        let html = $('body').html();
        project.publish_html = html.replace(new RegExp('ngif', 'g'), 'ngIf');
        //project.publish_html = $("body").html();
        res.json({ status: 200, data: project, message: 'success' });
      } else {
        res.json({ status: 301, data: project, message: 'Not found' });
      }
    });
};

// Get methode for main master page
exports.getHomeHtml = (req, res) => {
  models.page
    .findOne({ where: { template: 'master', isDeleted: 0 } })
    .then(project => {
      if (project != null) {
        const $ = cheerio.load(project.publish_html);
        $('a').each(function() {
          let href = $(this).attr('href');
          let url = '';
          if (href == '#' || href == 'javascript:void(0)') {
          } else {
            $(this).attr('(click)', `router('${href}')`);
            $(this).attr('href', 'javascript:void(0)');
          }
        });
        let html = $('body').html();
        project.publish_html = html.replace(new RegExp('ngif', 'g'), 'ngIf');
        //project.publish_html = $("body").html();
        res.json({ status: 200, data: project, message: 'success' });
      } else {
        res.json({ status: 301, data: project, message: 'Not found' });
      }
    });
};

// Get method for main master page
exports.getHomeHtml = (req, res) => {
  models.page
    .findOne({ where: { template: 'master', isDeleted: 0 } })
    .then(project => {
      if (project != null) {
        const $ = cheerio.load(project.publish_html);
        $('a').each(function() {
          let href = $(this).attr('href');
          let url = '';
          if (href == '#' || href == 'javascript:void(0)') {
          } else {
            $(this).attr('(click)', `router('${href}')`);
            $(this).attr('href', 'javascript:void(0)');
          }
        });
        let html = $('body').html();
        project.publish_html = html.replace(new RegExp('ngif', 'g'), 'ngIf');
        //project.publish_html = $("body").html();
        res.json({ status: 200, data: project, message: 'success' });
      } else {
        res.json({ status: 301, data: project, message: 'Not found' });
      }
    });
};

//Get method for page
exports.getpage = (req, res) => {
  let id = req.body.id;
  models.page
    .findOne({ where: { id: id } })
    .then(data => {
      res.json({ status: 200, message: 'success', data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

//upload method for page
exports.uplodimg = async (req, res) => {
  if (Object.keys(req.files.file).length == 0) {
    return res.status(400).send('No files were uploaded.');
  }

  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  let sampleFile = req.files.file;
  let type = sampleFile.mimetype.split('/')[1];
  let fileName = new Date().getTime();
  let image = await images.upload(
    sampleFile,
    './src/assets/images/' + fileName + '.' + type,
    false,
    './media/' + fileName + '.' + type
  );
  if (image.status == true) {
    res.send(
      'https://tripcenter.azureedge.net/cms/pages/images/' +
        fileName +
        '.' +
        type
    );
  } else {
    res.json({ status: false, message: 'Something went wrong' });
  }
};

// add method for page
exports.addpage = async (req, res) => {
  var draft_html = req.body.draft_html;
  let url = req.body.pagename.toString();
  url = url.toLowerCase().replace(/ /g, '-');
  models.page.findOne({ where: { url: url, isDeleted: 0 } }).then(project => {
    if (project == null) {
      fs.writeFile('./src/assets/demo/' + url + '.html', draft_html, err => {
        if (err) {
          res.json({ status: false, message: err });
        } else {
          // let html = await images.upload(sampleFile, './src/assets/images/bgimgs/' + fileName + '.' + type,false, './media/' + fileName + '.' + type);
          blobService.createBlockBlobFromLocalFile(
            'cms',
            './pages/html/' + url + '.html',
            './src/assets/demo/' + url + '.html',
            async err => {
              if (err) {
                res.json({ status: false, message: err });
              } else {
                let sampleFile = req.files.file;
                let type = sampleFile.mimetype.split('/')[1];
                let fileName = new Date().getTime();
                let image = await images.upload(
                  sampleFile,
                  './src/assets/images/' + fileName + '.' + type,
                  false,
                  './media/' + fileName + '.' + type
                );
                if (image.status == true) {
                  let create = {};
                  create = {
                    id: null,
                    draft_html: req.body.draft_html,
                    publish_html:
                      req.body.savetype == 'Draft'
                        ? null
                        : req.body.publish_html,
                    url: url,
                    pagename: req.body.pagename,
                    metatitle: req.body.metatitle,
                    metadescription: req.body.metadescription,
                    title: req.body.title,
                    subtitle: req.body.subtitle,
                    description: req.body.description,
                    meta_keywords: req.body.meta_keywords,
                    bgimage: 'media/' + fileName + '.' + type,
                    template: req.body.template
                  };
                  models.page
                    .create(create)
                    .then(function(elist) {
                      res.json({ status: 200, message: 'success' });
                    })
                    .catch(error => {
                      res.json({ status: false, message: error });
                    });
                } else {
                  res.json({ status: false, message: 'Something went wrong' });
                }
              }
            }
          );
        }
      });
    } else {
      res.json({ status: 301, message: 'Page already exists!' });
    }
    // project will be the first entry of the Projects table with the title 'aProject' || null
  });
};

// update method for page
exports.updatepage = (req, res) => {
  pagename = req.body.pagename;
  let url = pagename.toString();
  url = url.toLowerCase().replace(/ /g, '-');
  models.page
    .findOne({ where: { url: url, isDeleted: 0, id: { $ne: req.body.id } } })
    .then(async project => {
      if (project == null) {
        draft_html = req.body.draft_html;
        publish_html = req.body.publish_html;
        title = req.body.title;
        subtitle = req.body.subtitle;
        description = req.body.description;
        meta_keywords = req.body.meta_keywords;
        metatitle = req.body.metatitle;
        metadescription = req.body.metadescription;
        template = req.body.template;
        fs.writeFile('./src/assets/demo/' + url + '.html', draft_html, err => {
          if (err) {
            res.json({ status: 500, message: err });
          } else {
            blobService.createBlockBlobFromLocalFile(
              'cms',
              './pages/html/' + url + '.html',
              './src/assets/demo/' + url + '.html',
              async err => {
                if (err) {
                  res.json({ status: 500, message: err });
                } else {
                  let updateValues = {};
                  if (req.files != null) {
                    let sampleFile = req.files.file;
                    let type = sampleFile.mimetype.split('/')[1];
                    let fileName = new Date().getTime();

                    let image = await images.upload(
                      sampleFile,
                      './src/assets/images/' + fileName + '.' + type,
                      false,
                      './media/' + fileName + '.' + type
                    );
                    if (image.status == true) {
                      updateValues = {
                        url: url,
                        pagename: pagename,
                        draft_html: draft_html,
                        title: title,
                        subtitle: subtitle,
                        description: description,
                        bgimage: 'media/' + fileName + '.' + type,
                        metatitle: metatitle,
                        meta_keywords: meta_keywords,
                        metadescription: metadescription,
                        template: template
                      };
                      if (req.body.savetype == 'Publish')
                        updateValues.publish_html = publish_html;
                      save();
                    } else {
                      res.json({
                        status: false,
                        message: 'Something went wrong'
                      });
                    }
                  } else {
                    updateValues = {
                      url: url,
                      pagename: pagename,
                      draft_html: draft_html,
                      title: title,
                      subtitle: subtitle,
                      description: description,
                      metatitle: metatitle,
                      meta_keywords: meta_keywords,
                      metadescription: metadescription,
                      template: template
                    };
                    if (req.body.savetype == 'Publish')
                      updateValues.publish_html = publish_html;
                    save();
                  }

                  function save() {
                    models.page
                      .update(updateValues, { where: { id: req.body.id } })
                      .then(result => {
                        if (result) {
                          res.json({ status: 200, message: 'success' });
                        } else {
                          res.json({ status: false, message: 'Something went wrong' });
                        }
                      });
                  }
                }
              }
            );
          }
        });
      } else {
        res.json({ status: 301, message: 'Page already exists!' });
      }
    });
};

// delete method for page
exports.deletepage = (req, res) => {
  const id = req.body.id;
  updateValues = { isDeleted: 1 };
  models.page
    .update(updateValues, {
      where: { id: id }
    })
    .then(data => {
      res.json({ status: 200, message: 'success', data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};