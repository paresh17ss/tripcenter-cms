const models = require('../models');

// Get List method for category
exports.getcategorylist = (req,res)=>{
    models.categories.findAll({where:{isDeleted:0}}).then((data)=>{
            res.json({status:true,message:"success",data:data})
    }).catch((error)=>{
        res.json({status:false,message:error});
    })
}

// Add method for category
exports.addcategory = (req,res) =>{
    let name = req.body.categoryname;
    let url = req.body.categoryname.toString();
	url = url.toLowerCase().replace(/ /g, "-");
    models.categories.findOne({where:{name:name, isDeleted: 0}}).then(data=>{
        if(data == null){
            models.categories.create({name:name,url:url}).then((data)=>{
                res.json({ status: 200, message: 'success', data: data })
            }).catch((error)=>{
                res.json({ status: false, message: 'something went wrong', error})
            })
        }else{
            res.json({ status: 301, message: "Category already exists!" });
        }
    })
}

// Update method for category
exports.updatecategory  = (req,res) => {
    let id = req.body.id;
    let name = req.body.categoryname;
    models.categories.findOne({where:{ name:name, isDeleted: 0, id:{$ne: id}}}).then(data=>{
        if(data == null){
            let url = req.body.categoryname.toString();
            url = url.toLowerCase().replace(/ /g, "-");
            updateValues = { name: name };
            models.categories.findOne({ where: {  id:id } }).then((data) => {
                if(data){
                    models.categories.update(updateValues,{ where: { id: req.body.id } }).then((result) => {
                        if (result) {
                            res.json({ status: 200, message: "success", data: data });
                        } else {
                            res.json({ status: false, message: 'Something went wrong' });
                        }
                    });
                }
            }).catch((error) => {
                res.json({ status: false, message: error })
            });
        }
        else
        {
            res.json({ status: 301, message: "Category already exists!" });
        }
    });
};

// Delete method for category
exports.deletecategory = (req, res) => {
	const id = req.body.id;
	updateValues={isDeleted:1}
	models.categories.update(updateValues,{where: { id: id }
	}).then((data) => {
		res.json({ status: 200, message: 'success', data: data })
	}).catch((error) => {
		res.json({ status: false, message: error })
	});

};