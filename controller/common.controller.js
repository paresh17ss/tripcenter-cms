
var fs = require('fs')
var azure = require('azure-storage');
var blobService = azure.createBlobService('tripcenterindia', 'c84GhN5H5hNI7JbHYu+9KkcMmG3jOqV9PqnLw7vir9ceetEyNWYlejgf/BOKDgZ28tVkwGNvn/yMfNlxvOSU2w==');


// Global set image upload server side or locapath,
exports.upload = async(fileObject,filelocalpath,isDeletedlocal=false,filelivepath = null)=>{
    let resolve = new Promise((resolve,reject)=>{
        fileObject.mv(filelocalpath,function(err){
            if(err){
                resolve(err);
            }else{
                if(filelivepath != null){
                    blobService.createBlockBlobFromLocalFile('cms',filelivepath,filelocalpath,function(err){
                        if(err){
                            resolve(err);
                        }else{
                            if(isDeletedlocal == true){
                                fs.unlinkSync(filelocalpath)
                            }
                            resolve({status:true,filelocalpath,filelivepath});
                        }
                    })
                }else{
                    resolve({status:true,filelocalpath,filelivepath});
                    
                }               
            }
        })
    });

    return await resolve;
}