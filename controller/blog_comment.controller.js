const models = require("../models");
const moment = require("moment");
const sequelize = require("sequelize");
const images = require("../controller/common.controller");

//Get method for blog comment
exports.getCommentByBlogId = (req, res) => {
  let id = req.body.blog_id;
  models.blog_comment
    .findAll({
      where: { blog_id: id, isReply: false, isDeleted: false, status: 1 }
    })
    .then(data => {
      res.json({ status: true, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

//Get method for blog comment
exports.getCommentReplyByCommentId = (req, res) => {
  let id = req.body.comment_id;
  models.blog_comment
    .findAll({
      where: { belong_to_comment: id, isReply: true },
      order: [["createdAt", "ASC"]]
    })
    .then(reply => {
      res.json({ status: true, message: "success", data: reply });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

//Get list method for blog comment
exports.getCommentList = (req, res) => {
  models.sequelize
    .query(
      `SELECT bc.*,b.postname,(CASE WHEN bc.isReply=1 THEN (SELECT name FROM blog_comments WHERE id=bc.belong_to_comment) ELSE '' END) as InReplyTo FROM blog_comments bc INNER JOIN blogs b ON bc.blog_id = b.id WHERE bc.isDeleted=0`,
      { type: sequelize.QueryTypes.SELECT }
    )
    .then(data => {
      res.json({ status: true, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

//Add method for blog comment
exports.addComment = (req, res) => {
  let request = req.body;
  if (request.isReply) {
    request.status = 1;
  }
  models.blog_comment
    .create(request)
    .then(function(elist) {
      res.json({ status: true, message: "success", data: elist });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

//Update method for blog comment
exports.updateComment = (req, res) => {
  models.blog_comment
    .update(req.body, {
      where: { id: req.body.id }
    })
    .then(data => {
      res.json({ status: true, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

//Delete method for blog comment
exports.deleteComment = (req, res) => {
  const id = req.body.id;
  updateValues = { isDeleted: 1 };
  models.blog_comment
    .update(updateValues, {
      where: { id: id }
    })
    .then(data => {
      res.json({ status: true, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};
