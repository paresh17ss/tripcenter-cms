const models = require('../models');
var azure = require('azure-storage');
const fs = require('fs');
// create a blobservice of tripcenter
var blobService = azure.createBlobService('tripcenterindia', 'c84GhN5H5hNI7JbHYu+9KkcMmG3jOqV9PqnLw7vir9ceetEyNWYlejgf/BOKDgZ28tVkwGNvn/yMfNlxvOSU2w==');

//Get method for footer
exports.getfooter = (req,res) =>{
    let id = req.body.id
    models.footer.findOne({ where: { id: id } }).then((data) => {
        res.json({ status: 200, message: 'success', data: data })
    }).catch((error) => {
        res.json({ status: false, message: error })
    });
}

// Get list method for footer
exports.getfooterlist = (req,res) =>{
    models.footer.findAll({where:{isDeleted:'0'}}).then((data)=>{
        res.json({status:200,message:'success',data:data})
    }).catch((err)=>{
        res.json({status:false,message:'Something went wrong',err})
    })
}

// Add method for footer 
exports.addfooter = (req, res) => {
    var data = req.body.html
    let url = req.body.footername.toString();
    url = url.toLowerCase().replace(/ /g, "-");
    models.footer.findOne({ where: { footer_name: req.body.footername, isDeleted: 0 } }).then(project => {
        if (project == null) {
            
            fs.writeFile('./src/assets/footer/' + url + '.html', data, (err) => {
                if (err) {
                    res.json({ status: false, message: err })
                } else {

                    blobService.createBlockBlobFromLocalFile('cms', './footer/' + url + '.' + 'html', './src/assets/footer/' + url + '.html', (err) => {

                        if (err) {
                            res.json({ status: false, message: err })
                        } else {
                            models.footer.create({ id: null, html: req.body.html,url:url, footer_name: req.body.footername }).then(function (data) {
                                res.json({ status: 200, message: "success", data: data });
                            }).catch((error) => {
                                res.json({ status: false, message: error })
                            });
                        }

                    })
                }
            });

        } else {
            res.json({ status: 301, message: 'Footer already exists' })
        }
    })

}

// Update method for footer 
exports.updatefooter = (req,res) =>{
    var data = req.body.html
    let url = req.body.footername.toString();
    url = url.toLowerCase().replace(/ /g, "-");
    models.footer.findOne({ where: { footer_name: req.body.footername, isDeleted: 0, id: { $ne: req.body.id } } }).then(project => {
        if (project == null) {
	fs.writeFile('./src/assets/footer/' + url + '.html', data, (err) => {
		if (err) {
			res.json({ status: 500, message: err });
		} else {
            // footer upload direct to bulck blob 
			blobService.createBlockBlobFromLocalFile('cms', './footer/' + url + '.' + 'html', './src/assets/footer/' + url + '.html', (err) => {
				if (err) {
					res.json({ status: 500, message: err });
				} else {
					// let updateValues = {};
						updateValues = { html: req.body.html, footer_name: url };
                        save();
                        
					function save() {
						models.footer.update(updateValues, { where: { id: req.body.id } }).then((result) => {
							if (result) {
								res.json({ status: 200, message: "success" });
							} else {
								res.json({status:false,message:"Something went wrong"})
							}
						});
					}
				}
			});
		}
    });
} else{
    res.json({status:301,message:"Footer already exists"})
}  
    });
};

// Delete method for footer
exports.deletefooter = (req,res)=>{
    const id = req.body.id;
	updateValues={isDeleted:1}
	models.footer.update(updateValues,{where: { id: id }
	}).then((data) => {
		res.json({ status: 200, message: 'success', data: data })
	}).catch((error) => {
		res.json({ status: false, message: error })
	});
};