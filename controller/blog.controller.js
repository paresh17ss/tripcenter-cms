const models = require("../models");
const moment = require("moment");
const sequelize = require("sequelize");
const images = require("../controller/common.controller");

// Get method for blog
exports.getblog = (req, res) => {
  let id = req.body.id;
  models.blog
    .findOne({ where: { id: id } })
    .then(data => {
      res.json({ status: 200, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

// Get list method for blog
exports.getbloglist = (req, res) => {
  models.blog
    .findAll({
      where: { isDeleted: 0 },
      attributes: [
        "postname",
        "tag",
        "id",
        "status",
        "visibility",
        "createdAt",
        "feature_image",
        [
          sequelize.literal(`STUFF((SELECT ', ' + CONVERT(varchar(50),ISNULL(categories.name,''))  from categories 
where categories.id IN(SELECT category_id FROM blog_category_mappings WHERE  blog_id = blog.id) 
FOR XML PATH('')), 1, 1, '')`),
          "category_name"
        ]
      ]
    })
    .then(data => {
      res.json({ status: 200, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

// Get method for blog
exports.frontendbloglist = (req, res) => {
  let page = req.body.page;
  let perPage = 3;
  models.sequelize
    .query(
      `SELECT overall_count = COUNT(*) OVER(),*,category_name = STUFF((SELECT ', ' + CONVERT(varchar(50),ISNULL(categories.name,''))  from categories where categories.id IN(SELECT category_id FROM blog_category_mappings WHERE  blog_id = blogs.id) FOR XML PATH('')), 1, 1, '') FROM blogs 
      where blogs.isdeleted = 0 and blogs.visibility = 'Public' and blogs.status = 'Publish' and publish_date <= cast (GETDATE() as DATE) 
      ORDER BY publish_date DESC OFFSET ${page *
        perPage} ROWS FETCH NEXT ${perPage} ROWS ONLY`,
      { type: sequelize.QueryTypes.SELECT }
    )
    .then(data => {
      res.json({ status: 200, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

// Get method for blog
exports.frontendblogdetail = (req, res) => {
  models.blog
    .findOne({
      where: { isDeleted: 0, url: decodeURIComponent(req.body.url), visibility: 'Public', status:'Publish', publish_date: {$lte: moment()} },
      attributes: [
        "postname",
        "tag",
        "id",
        "publishhtml",
        "status",
        "visibility",
        "createdAt",
        "feature_image",
        [
          sequelize.literal(`STUFF((SELECT ', ' + CONVERT(varchar(50),ISNULL(categories.name,''))  from categories 
where categories.id IN(SELECT category_id FROM blog_category_mappings WHERE  blog_id = blog.id) 
FOR XML PATH('')), 1, 1, '')`),
          "category_name"
        ]
      ]
    })
    .then(data => {
      res.json({ status: 200, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};


// Get Popular Blog list
exports.GetPopularBlogList = (req, res) => {
  let blog_id = req.body.blog_id;
  let qryStr = `SELECT * FROM blogs WHERE blogs.isdeleted = 0 and blogs.visibility = 'Public' and blogs.status = 'Publish' and publish_date <= cast (GETDATE() as DATE) and id in (SELECT blog_id FROM blog_category_mappings WHERE category_id in (SELECT category_id FROM blog_category_mappings WHERE blog_id=${blog_id})) and id != ${blog_id}`;
  models.sequelize
    .query(qryStr, { type: sequelize.QueryTypes.SELECT })
    .then(data => {
      res.json({ status: 200, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

// Get Latest Blog list
exports.GetLatestBlogList = (req, res) => {
  models.blog
    .findAll({
      where: [{ isDeleted: 0, visibility: "Public", status: "Publish", publish_date: {$lte: moment()} }],
      order: [["publish_date", "DESC"]],
      limit: 4
    })
    .then(data => {
      res.json({ status: 200, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

// Add method for blog
exports.addblog = (req, res) => {
  let url = req.body.blogname.toString().trim();
  url = url.toLowerCase().replace(/ /g, "-");
  let publish_date = moment(req.body.publish_date).format("YYYY-MM-DD");
  models.blog.findOne({ where: { url: url, isDeleted: 0 } }).then(async project => {
    if (project == null) {
      let sampleFile = req.files.file;
      let type = sampleFile.mimetype.split("/")[1];
      let fileName = new Date().getTime();
      let image = await images.upload(
        sampleFile,
        "./src/assets/images/" + fileName + "." + type,
        true,
        "./media/" + fileName + "." + type
      );
      if (image.status == true) {
        let c = req.body.categories;
        let categorie = c.split(",");
        let create = {
          user_id: null,
            url: url,
            categories: req.body.categories,
            postname: req.body.blogname,
            tag: req.body.tag,
            drafthtml: req.body.html,
            status: req.body.status,
            visibility: req.body.visibility,
            meta_title: req.body.meta_title,
            meta_description: req.body.meta_description,
            feature_image: "./media/" + fileName + "." + type
        };
        if (req.body.status == "Publish") {
          create.publishhtml = req.body.html;
          create.publish_date = publish_date;
          create.publish_on = req.body.publish_on;
        } 
        models.blog
          .create(create)
          .then(function(elist) {
            let blog_category = [];
            categorie.forEach(id => {
              blog_category.push({ blog_id: elist.id, category_id: id });
            });
            // Add blogpage to bulk
            models.blog_category_mapping.bulkCreate(blog_category, {
              returning: true
            });
            res.json({ status: 200, message: "success", data: elist });
          })
          .catch(error => {
            res.json({ status: false, message: error });
          });
      } else {
        res.json({ status: false, message: "Something went wrong" });
      }
    } else {
      res.json({ status: 301, message: "Blog already exists!" });
    }
  });
};

// Update method for blog
exports.updateblog = (req, res) => {
  let blogname = req.body.blogname;
  let blog_id = req.body.id;
  let url = blogname.toString().trim();
  url = url.toLowerCase().replace(/ /g, "-");
  models.blog.findOne({ where: { url: url, isDeleted: 0, id: {$ne: blog_id} } }).then(async project => {
    if (project == null) {
      let updateValues = {};
      models.blog_category_mapping
        .destroy({ where: { blog_id: blog_id } })
        .then(async data => {
          let publish_date = moment(req.body.publish_date).format("YYYY-MM-DD");
          updateValues = {
            user_id: null,
                url: url,
                postname: req.body.blogname,
                tag: req.body.tag,
                drafthtml: req.body.html,
                status: req.body.status,
                visibility: req.body.visibility,
                meta_title: req.body.meta_title,
                meta_description: req.body.meta_description,
                categories: req.body.categories
          };
          if (req.body.status == "Publish") {
            updateValues.publishhtml = req.body.html;
            updateValues.publish_date = publish_date;
            updateValues.publish_on = req.body.publish_on;
          } 
          if (req.files != null) {
            let sampleFile = req.files.file;
            let type = sampleFile.mimetype.split("/")[1];
            let fileName = new Date().getTime();
            // image upload local and bulk serverside
            let image = await images.upload(
              sampleFile,
              "./src/assets/images/" + fileName + "." + type,
              true,
              "./media/" + fileName + "." + type
            );
            if (image.status == true) {
              updateValues.feature_image = "./media/" + fileName + "." + type;
              }
            }
            save();
        });
        
        function save()
        {
          models.blog
              .update(updateValues, { where: { id: blog_id } })
              .then(result => {
                if (result) {
                  let blog_category = [];
                  let c = req.body.categories;
                  let categorie = c.split(",");
                  categorie.forEach(id => {
                    blog_category.push({
                      blog_id: blog_id,
                      category_id: id
                    });
                  });
                  models.blog_category_mapping.bulkCreate(blog_category, {
                    returning: true
                  });
                  res.json({ status: 200, message: "success" });
                } else {
                  res.json({ status: false, message: "Something went wrong"  });
                }
              });
        }
    } else {
      res.json({ status: 301, message: "Blog already exists!" });
    }
    });
};

// Delete method for blog
exports.deleteblog = (req, res) => {
  const id = req.body.id;
  updateValues = { isDeleted: 1 };
  models.blog
    .update(updateValues, {
      where: { id: id }
    })
    .then(data => {
      res.json({ status: 200, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};