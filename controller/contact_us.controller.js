const models = require("../models");
const moment = require("moment");
const sequelize = require("sequelize");
const images = require("../controller/common.controller");

//Get List method for contact-us
exports.getContactUsList = (req,res)=>{
  models.contactUs.findAll({where:{isDeleted:0}}).then((data)=>{
          res.json({status:true,message:"success",data:data})
  }).catch((error)=>{
      res.json({status:false,message:error});
  })
}

// Add method for contact-us
exports.addContactUs = (req, res) => {
  models.contactUs.create(
    {
      firstName:req.body.firstName, 
      lastName:req.body.lastName, 
      email:req.body.email,
      phoneNo: req.body.phoneNo,
      subject: req.body.subject,
      interestedIn: req.body.subject == 'Something Else' ? req.body.interestedIn : null,
      booking: req.body.subject == 'Something Else' ? req.body.booking : null,
      message: req.body.message
    }).then((data)=>{
      res.json({status:200,message:'success',data:data})
  }).catch((error)=>{
      res.json({status:false,message:'Something went wrong',error})
  })
};

// Update method for contact-Us
exports.updateContactUs  = (req,res) => {
  let id = req.body.id;
  updateValues = { 
    firstName: req.body.firstName
      ,lastName: req.body.lastName
      ,email: req.body.email
      ,phoneNo: req.body.phoneNo
      ,subject: req.body.subject
      ,interestedIn: req.body.subject == 'Something Else' ? req.body.interestedIn : null
      ,booking: req.body.subject == 'Something Else' ? req.body.booking : null
      ,message: req.body.message
  };
  models.contactUs.findOne({ where: {  id:id } }).then((data) => {
    if(data){
        models.contactUs.update(updateValues,{ where: { id: id } }).then((result) => {
            if (result) {
                res.json({ status: 200, message: "success", data: data });
            } else {
                res.json({ status: false, message: 'Something went wrong' });
            }
        });
    }
  }).catch((error) => {
      res.json({ status: false, message: error })
  });
}

// Delete method for contact-us
exports.deleteContactUs = (req, res) => {
	const id = req.body.id;
	updateValues = { isDeleted : 1 } 
	models.contactUs.update(updateValues , {where: { id: id }
	}).then((data) => {
		res.json({ status: 200, message: 'success', data: data })
	}).catch((error) => {
		res.json({ status: false, message: error })
	});
};

