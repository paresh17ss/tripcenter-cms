const models = require("../models");
const moment = require("moment");
const sequelize = require("sequelize");
const images = require("../controller/common.controller");

//Get method for news
exports.getnews = (req, res) => {
  let id = req.body.id;
  models.news
    .findOne({ where: { id: id } })
    .then(data => {
      res.json({ status: 200, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

//Get list method for news
exports.getnewslist = (req, res) => {
  models.news
    .findAll({
      where: { isDeleted: 0 },
      attributes: [
        "postname",
        "tag",
        "id",
        "status",
        "visibility",
        "createdAt",
        "feature_image",
        [
          sequelize.literal(`STUFF((SELECT ', ' + CONVERT(varchar(50),ISNULL(categories.name,''))  from categories 
where categories.id IN(SELECT category_id FROM news_category_mappings WHERE  news_id = news.id) 
FOR XML PATH('')), 1, 1, '')`),
          "category_name"
        ]
      ]
    })
    .then(data => {
      res.json({ status: 200, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

//Get list method for news
exports.frontendnewslist = (req, res) => {
  let page = req.body.page;
  let perPage = 3;
  models.sequelize
    .query(
      `SELECT overall_count = COUNT(*) OVER(),*,category_name = STUFF((SELECT ', ' + CONVERT(varchar(50),ISNULL(categories.name,''))  from categories where categories.id IN(SELECT category_id FROM news_category_mappings WHERE  news_id = news.id) FOR XML PATH('')), 1, 1, '') FROM news 
      where news.isdeleted = 0 and news.visibility = 'Public' and news.status = 'Publish' and publish_date <= cast (GETDATE() as DATE)
      ORDER BY publish_date DESC OFFSET ${page *
        perPage} ROWS FETCH NEXT ${perPage} ROWS ONLY`,
      { type: sequelize.QueryTypes.SELECT }
    )
    .then(data => {
      res.json({ status: 200, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

//Get list method for news
exports.frontendnewsdetail = (req, res) => {
  models.news
    .findOne({
      where: { isDeleted: 0, url: decodeURIComponent(req.body.url), visibility: 'Public', status:'Publish', publish_date: {$lte: moment()}  },
      attributes: [
        "postname",
        "tag",
        "id",
        "publishhtml",
        "status",
        "visibility",
        "createdAt",
        "feature_image",
        [
          sequelize.literal(`STUFF((SELECT ', ' + CONVERT(varchar(50),ISNULL(categories.name,''))  from categories 
where categories.id IN(SELECT category_id FROM news_category_mappings WHERE  news_id = news.id) 
FOR XML PATH('')), 1, 1, '')`),
          "category_name"
        ]
      ]
    })
    .then(data => {
      res.json({ status: 200, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

// Get Popular news list method
exports.GetPopularNewsList = (req, res) => {
  let news_id = req.body.news_id;
  let qryStr = `SELECT * FROM news WHERE news.isdeleted = 0 and news.visibility = 'Public' and news.status = 'Publish' and publish_date <= cast (GETDATE() as DATE) and id in (SELECT news_id FROM news_category_mappings WHERE category_id in (SELECT category_id FROM news_category_mappings WHERE news_id=${news_id})) and id != ${news_id}`;
  models.sequelize
    .query(qryStr, { type: sequelize.QueryTypes.SELECT })
    .then(data => {
      res.json({ status: 200, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

// Get Latest News list method
exports.GetLatestNewsList = (req, res) => {
  models.news
    .findAll({
      where: [{ isDeleted: 0, visibility: "Public", status: "Publish", publish_date: {$lte: moment()} }],
      order: [["publish_date", "DESC"]],
      limit: 4
    })
    .then(data => {
      res.json({ status: 200, message: "success", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

// Add method for news
exports.addnews = (req, res) => {
  let url = req.body.newsname.toString().trim();
  url = url.toLowerCase().replace(/ /g, "-");
  let publish_date = moment(req.body.publish_date).format("YYYY-MM-DD");
  models.news.findOne({ where: { url: url, isDeleted: 0 } }).then(async project => {
    if (project == null) {
      let sampleFile = req.files.file;
      let type = sampleFile.mimetype.split("/")[1];
      let fileName = new Date().getTime();
      let image = await images.upload(
        sampleFile,
        "./src/assets/images/" + fileName + "." + type,
        true,
        "./media/" + fileName + "." + type
      );
      if (image.status == true) {
        let c = req.body.categories;
        let categorie = c.split(",");
        let create = {
            user_id: null,
            url: url,
            categories: req.body.categories,
            postname: req.body.newsname,
            tag: req.body.tag,
            drafthtml: req.body.html,
            status: req.body.status,
            visibility: req.body.visibility,
            meta_title: req.body.meta_title,
            meta_description: req.body.meta_description,
            feature_image: "./media/" + fileName + "." + type
          };
          if (req.body.status == "Publish") {
            create.publishhtml = req.body.html;
            create.publish_date = publish_date;
            create.publish_on = req.body.publish_on;
          } 
        models.news
          .create(create)
          .then(function(elist) {
            let news_category = [];
            categorie.forEach(id => {
              news_category.push({ news_id: elist.id, category_id: id });
            });
            models.news_category_mapping.bulkCreate(news_category, {
              returning: true
            });
            res.json({ status: 200, message: "success", data: elist });
          })
          .catch(error => {
            res.json({ status: false, message: error });
          });
      } else {
        res.json({ status: false, message: "Something went wrong" });
      }
    } else {
      res.json({ status: 301, message: "News already exists!" });
    }
  });
};

// update method for news
exports.updatenews = (req, res) => {
  let news_id = req.body.id;
  let newsname = req.body.newsname;
  let url = newsname.toString();
  url = url.toLowerCase().replace(/ /g, "-");
  models.news.findOne({ where: { url: url, isDeleted: 0, id: { $ne: news_id } }})
    .then(async project => {
      if (project == null) {
        let updateValues = {};
        models.news_category_mapping
          .destroy({ where: { news_id: news_id } })
          .then(async data => {
            let publish_date = moment(req.body.publish_date).format("YYYY-MM-DD");
            updateValues = {
              user_id: null,
                  url: url,
                  postname: req.body.newsname,
                  tag: req.body.tag,
                  drafthtml: req.body.html,
                  status: req.body.status,
                  visibility: req.body.visibility,
                  meta_title: req.body.meta_title,
                  meta_description: req.body.meta_description,
                  categories: req.body.categories
            };
            if (req.body.status == "Publish") {
              updateValues.publishhtml = req.body.html;
              updateValues.publish_date = publish_date;
              updateValues.publish_on = req.body.publish_on;
            } 
            if (req.files != null) {
              let sampleFile = req.files.file;
              let type = sampleFile.mimetype.split("/")[1];
              let fileName = new Date().getTime();
              let image = await images.upload(
                sampleFile,
                "./src/assets/images/" + fileName + "." + type,
                true,
                "./media/" + fileName + "." + type
              );
              if (image.status == true) {
                updateValues.feature_image = "./media/" + fileName + "." + type;
                
                save();
              } else {
                res.json({ status: false, message: "Something went wrong" });
              }
            } 
              save();
            });

            function save() {
              models.news
                .update(updateValues, { where: { id: req.body.id } })
                .then(result => {
                  if (result) {
                    let news_category = [];
                    let c = req.body.categories;
                    let categorie = c.split(",");
                    categorie.forEach(id => {
                      news_category.push({
                        news_id: req.body.id,
                        category_id: id
                      });
                    });
                    models.news_category_mapping.bulkCreate(news_category, {
                      returning: true
                    });

                    res.json({ status: 200, message: 'Success' });
                  } else {
                    res.json({ status: false, message: "Something went wrong"  });
                  }
                });
            }
      } else {
        res.json({ status: 301, message: "News already exists!" });
      }
    });
};

// delete method for news
exports.deletenews = (req, res) => {
  const id = req.body.id;
  updateValues = { isDeleted: 1 };
  models.news
    .update(updateValues, {
      where: { id: id }
    })
    .then(data => {
      res.json({ status: 200, message: "successfully", data: data });
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};
