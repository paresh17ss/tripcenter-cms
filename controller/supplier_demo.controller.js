const models = require("../models");
const sequelize = require("sequelize");
const images = require("./common.controller");

// method for new Supplier Demo
exports.newSupplierDemo = (req, res) => {
  let data = (
    {
      firstName:req.body.firstName, 
      lastName:req.body.lastName, 
      companyname:req.body.companyname,
      fleetsize: req.body.fleetsize,
      email:req.body.email,
      phoneNo: req.body.phoneNo,
      country: req.body.country,
      question: req.body.question,
      urself: req.body.urself,
      important: req.body.important,
      startselling: req.body.startselling,
      hearfrom: req.body.hearfrom
    });
    res.json({ status: true, message: 'success', data: data});
};

