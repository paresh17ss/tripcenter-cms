const models = require('../models');
const sequelize = require('sequelize');

//Get list method for user
exports.getuserlist = (req, res) => {
  models.users
    .findAll({ include: 'roles' })
    .then(data => {
      res.json({ status: 200, message: 'success', data: data });
    })
    .catch(err => {
      res.json({ status: 500, message: 'Something went wrong', err });
    });
};

//Add method for user
exports.adduser = (req, res) => {
  models.users.findOne({ where: { Email: req.body.email } }).then(data => {
    if (data == null) {
      models.users
        .create({
          fName: req.body.fName,
          lName: req.body.lName,
          email: req.body.email,
          telPhone: req.body.telPhone,
          password: req.body.password,
          role: req.body.role,
          dateCreated: req.body.dateCreated
        })
        .then(data => {
          res.json({ status: 200, message: 'success', data: data });
        })
        .catch(err => {
          res.json({ status: false, message: 'Something went wrong', err });
        });
    } else {
      res.json({ status: 301, message: 'Email already exists!' });
    }
  });
};

//update method for user
exports.updateuser = (req, res) => {
  let id = req.body.id;
  updateValues = {
    fName: req.body.fName,
    lName: req.body.lName,
    email: req.body.email,
    telPhone: req.body.telPhone,
    password: req.body.password,
    role: req.body.role
  };
  models.users
    .findOne({ where: { Email: req.body.email, id: { $ne: id } } })
    .then(data => {
      if (data == null) {
        models.users
          .update(updateValues, { where: { id: id } })
          .then(result => {
            if (result) {
              res.json({ status: 200, message: 'success' });
            } else {
              res.json({ status: false, message: 'Something went wrong' });
            }
          });
      } else {
        res.json({ status: 301, message: 'Email already exists!' });
      }
    })
    .catch(error => {
      res.json({ status: false, message: error });
    });
};

//Delete method for user
exports.deleteuser = (req, res) => {
  // const id = req.body.id;
  // updateValues={isDeleted:1}
  // models.users.update(updateValues,{where: { id: id }
  // }).then((data) => {
  res.json({ status: 200, message: 'success', data: data });
  // }).catch((error) => {
  // 	res.json({ status: false, message: error })
  // });
};

//Authenticate method for user
exports.loginUser = (req, res) => {
  models.users
    .findOne({ where: { email: req.body.email } })
    .then(data => {
      if (data != null) {
        if (data.password == req.body.password) {
          res.json({ status: 200, data: data });
        } else {
          res.json({ status: false, message: 'Incorrect password' });
        }
      } else {
        res.json({ status: false, message: 'Email does not exist' });
      }
    })
    .catch(error => {
      res.json({ status: false, message: 'Something went wrong' });
    });
};

exports.logoutUser = (req, res) => {
  // remove user from local storage to log user out
  localStorage.removeItem('currentUser');
};
