import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewscommentlistComponent } from './Newscommentlist.component';

describe('NewscommentlistComponent', () => {
  let component: NewscommentlistComponent;
  let fixture: ComponentFixture<NewscommentlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewscommentlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewscommentlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
