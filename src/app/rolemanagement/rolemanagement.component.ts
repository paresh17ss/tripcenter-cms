import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../app-setting';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import{ FormGroup, FormControl, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';

declare var $: any;
@Component({
  selector: 'app-rolemanagement',
  templateUrl: './rolemanagement.component.html',
  styleUrls: ['./rolemanagement.component.scss']
})
export class RolemanagementComponent implements OnInit {
 private readonly notifier: NotifierService;
 data: any = [];
  id: any;
  submitted: boolean = false;
  
  public savedForm: FormGroup = new FormGroup({
    key: new FormControl('', Validators.required),
    displayValue: new FormControl('', Validators.required),
    isSuperUser: new FormControl(false),
 });

 public editForm: FormGroup = new FormGroup({
  key: new FormControl('', Validators.required),
  displayValue: new FormControl('', Validators.required),
  isSuperUser: new FormControl(),
 })

  constructor(private http: HttpClient, private router: Router
    , private spinner: NgxSpinnerService, private notifierService: NotifierService) {
      this.notifier = notifierService;
    }

  ngOnInit() {
     this.getRoleList();
  }

  /* Getting a role listing*/
  getRoleList()
  {
    this.http.get(AppSettings.API_ENDPOINT + "rolelist").subscribe((res: any) => {
      if (res.status == true) {
        this.data = res.data;
      } else {
        this.notifier.notify('error', res.message);
      }
    });
  }

  /* Open a modal for add new role */
  addrole() {
    $('#message-modal').modal('show')
  }

   /*Submiting a addcategoriesform */
   onSubmit(data){
    if (this.savedForm.invalid) {
      this.submitted = true;
    } else {
      let formData = new FormData()
      for (let d in this.savedForm.value) {
        formData.append(d, this.savedForm.value[d])
      }
      /*Add new categorie*/
      return this.http.post(AppSettings.API_ENDPOINT + '/addrole', formData).subscribe((response: any) => {
        this.spinner.hide();
        if (response.status == 200) {
          $("#message-modal").modal("hide");
          this.getRoleList();
          this.notifier.notify( 'success', 'Added successfully' );
        }
        else {
          this.notifier.notify( 'error', response.message );
        }
      });
    }
  }

  /* Open modal for edit or update a role */
  editrole(data){
    this.id = data.id;
    this.editForm.controls['key'].setValue(data.key);
    this.editForm.controls['displayValue'].setValue(data.displayValue);
    this.editForm.controls['isSuperUser'].setValue(data.isSuperUser);
    $('#edit-modal').modal('show')
  }

  /* Edit or update a role */
  editsave(data){
    if (this.editForm.invalid) {
      this.submitted = true;
    } else {
      let formData = new FormData()
      for (let d in this.editForm.value) {
        formData.append(d, this.editForm.value[d])
      }
      formData.append('id', this.id);
      return this.http.post(AppSettings.API_ENDPOINT + '/editrole', formData).subscribe((res:any)=>{
        if(res.status == 200){
          $('#edit-modal').modal('hide')
          this.getRoleList();
          this.notifier.notify( 'success', 'Updated successfully' );
        } else {
          this.notifier.notify('error', res.message);
        }
      })
    }
  }  
}
