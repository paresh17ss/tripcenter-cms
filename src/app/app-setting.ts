export class AppSettings {
   // public static API_ENDPOINT='https://tripcenter-cms.azurewebsites.net/';
  public static API_ENDPOINT = 'http://localhost:4000/api/';
  //public static HTMLPAGE_URL ='https://tripcenter.azureedge.net/cms/pages/html/';
  public static HTMLPAGE_URL = 'assets/demo/';
  // image-path
  public static IMAGE_PATH = 'https://tripcenter.azureedge.net/cms/';
 }