import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {NotifierService} from 'angular-notifier';
import {AppSettings} from '../app-setting';
import {NgxSpinnerService} from 'ngx-spinner';
declare var $: any;
import { HttpClient, } from '@angular/common/http';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})

export class NewsComponent implements OnInit {
  private readonly notifier: NotifierService;
  selectedvalue: string = 'city';
  data:any=[];
  id:any;
  path:any = AppSettings.IMAGE_PATH;
  
  constructor(private notifierService:NotifierService,private http: HttpClient,private router: Router,private spinner:NgxSpinnerService) {
    this.notifier = notifierService;
   }

  ngOnInit() {
     this.getNewsList();
  }

  /*Getting a newslist*/
  getNewsList() {
    this.http.get(AppSettings.API_ENDPOINT + "/newslist").subscribe((res: any) => {
      if (res.status == 200) {
        this.spinner.hide()
        this.data = res.data;
      } else {
        this.notifier.notify( 'error', 'Something went wrong' );
      }
    })
  }

  /*Go for add new page*/
  newpage(){
    this.router.navigate(['newspage/']);
  }

  /*Go for edit news page*/
  editblog(val) {
    this.id = val;
    let data = btoa(val)
    this.router.navigate(['editnewspage/'+data]);
  }

  /*Delete a newpage*/
  deletenews(){
    this.http.post(AppSettings.API_ENDPOINT + "/deletenews",{"id":this.id}).subscribe((res:any)=>{
        if(res.status == 200){
          $('#delete-modal').modal('hide');
          this.getNewsList();
          this.notifier.notify('success','Deleted successfully');
        }else{
          this.notifier.notify('error','Something went wrong')
        }
    })
  }

  /*Open modal for delete newpage*/
  delete(id){
    this.id = id,
    $('#delete-modal').modal('show');
  }

  selectChangeHandler (event: any) {
    this.selectedvalue = event.target.value;
  }
}
