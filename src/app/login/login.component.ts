import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NotifierService } from 'angular-notifier';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../app-setting';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../shared/services/AuthenticationService';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  returnUrl: string;
  private readonly notifier: NotifierService;
  submitted: boolean = false;
  rememberMe: boolean;
  login = {
    email: '',
    password: ''
  };

  public loginForm: FormGroup = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    rememberMe: new FormControl('')
  });

  constructor(
    private http: HttpClient,
    public notifierService: NotifierService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.notifier = notifierService;
  }

  ngOnInit() {
    var id = this.getCookie('email');
    var pwd = this.getCookie('password');
    if (id != '' && pwd != '') {
      this.rememberMe = true;
      this.login.email = id;
      this.login.password = pwd;
    }

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  /*Submit login form*/
  onSubmit() {
    if (this.loginForm.invalid) {
      this.submitted = true;
    } else {
      this.spinner.show();

      this.login.email = this.loginForm.controls.email.value;
      this.login.password = this.loginForm.controls.password.value;

      //stop here if form is invalid
      if (!this.login.email && !this.login.password) {
        return;
      }
      debugger;
      this.authenticationService
        .login(this.login.email, this.login.password)
        .pipe(first())
        .subscribe(
          data => {
            if (!data.status) {
              this.notifier.notify('error', data.message);
            } else {
              if (this.rememberMe) {
                this.setCookie('email', this.login.email, 365);
                this.setCookie('password', this.login.password, 365);
              } else {
                this.setCookie('email', this.login.email, -1);
                this.setCookie('password', this.login.password, -1);
              }
              this.router.navigate(['/pagelist']);
            }
            // this.router.navigate([this.returnUrl]);
            this.spinner.hide();
          },
          error => {
            this.notifier.notify('error', error.message);
            this.spinner.hide();
          }
        );

      // if (this.loginForm.invalid) {
      //   this.submitted = true;
      // } else {
      //   let formData = new FormData()
      //   for (let d in this.loginForm.value) {
      //     formData.append(d, this.loginForm.value[d])
      //   }
      //   /*Add new categorie*/
      //   return this.http.post(AppSettings.API_ENDPOINT + '/loginUser', formData).subscribe((response: any) => {
      //     if (response.status == true) {
      //       this.router.navigate([this.returnUrl]);
      //       this.notifier.notify( 'success', 'Logged-in successfully' );
      //     } else if (response.status == 302) {
      //       this.notifier.notify( 'error', response.message );
      //     } else {
      //       this.spinner.hide();
      //       this.notifier.notify( 'error', 'Something went wrong' );
      //     }
      //   });
      // }
    }
  }

  setCookie = (cname, cvalue, exdays) => {
    var d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    var expires = 'expires=' + d.toUTCString();
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
  };

  getCookie = cname => {
    var name = cname + '=';
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  };
}
