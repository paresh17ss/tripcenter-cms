import { Component, OnInit, Inject, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { AppSettings } from '../app-setting';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
declare var $: any;
import { HttpClient, } from '@angular/common/http';
declare var Vvveb: any;

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { getListeners } from '@angular/core/src/render3/discovery_utils';
@Component({
  selector: 'app-page-list',
  templateUrl: './page-list.component.html',
  styleUrls: ['./page-list.component.scss']
})

export class PageListComponent implements OnInit {
  public cloneForm: FormGroup = new FormGroup({
    pagename: new FormControl('', Validators.required),
    title: new FormControl('', Validators.required),
    subtitle: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    bgimage: new FormControl('', Validators.required),
    metatitle: new FormControl('', Validators.required),
    metadescription: new FormControl('', Validators.required),
    meta_keywords: new FormControl(''),
    draft_html: new FormControl(''),
    publish_html: new FormControl(''),
    template: new FormControl('')
  });

  path: any = AppSettings.IMAGE_PATH;
  dtOptions: any;
  data: any = [];
  page: any = [];
  file: any;
  id: any;
  public searchTxt: any;
  private readonly notifier: NotifierService;
  selectedvalue: string = 'home';
  templateFilter: string = 'home';
  savetype: any;
  submitted: boolean = false;
  
  constructor(private notifierService: NotifierService, private spinner: NgxSpinnerService, private http: HttpClient, private _renderer2: Renderer2, @Inject(DOCUMENT) private _document, private router: Router) {
    this.notifier = notifierService;
  }

  ngOnInit() {
    this.getList();
    $(document).on('click', '.table-edit', function (res, data) {
      this.data = {
        ajax: {
           /*Get a page listing*/
          url: AppSettings.API_ENDPOINT + "/pagelist",
          type: 'POST',
          param: {templateFilter : this.templateFilter},
          success: function (result) { },
          data: function (data) {
            return JSON.stringify(data);
          }
        },
      }
    });
  }

  /*Get a pages listing*/
  getList() {
    this.http.post(AppSettings.API_ENDPOINT + "/pagelist", {templateFilter : this.templateFilter}).subscribe((res: any) => {
      if (res.status == 200) {
        this.data = res.data;
        this.spinner.hide()
      } else {
        this.notifier.notify('error', 'Something went wrong');
      }
    })
  }

  /*Go for edit or update page*/
  tedit(val) {
    this.page = val;
    let pagename = btoa(val)
    this.router.navigate(['editpage/' + pagename]);
  }

  /*Open modal for select templet*/
  newpage() {
    $('#new-page-modal').modal('show')
  }
  
  /*Go for a add new page*/
  pageopen() {
    $('#new-page-modal').modal('hide')
    let templateUrl = btoa(this.selectedvalue)
    this.router.navigate(['addnewpage/' + templateUrl], { state: { templateUrl: this.selectedvalue } });
  }

  //event handler for the select element's change event
  selectChangeHandler(event: any) {
    //update the ui
    this.selectedvalue = event.target.value;
    this.cloneForm.controls['template'].setValue(this.selectedvalue);
  }

  //event handler for the select element's change event
  selectChangeTemplateHandler(event: any) {
    //update the ui
    this.templateFilter = event.target.value;
    this.getList();
  }

  /*event handler for the image file change*/
  onFileChange(event) {
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];

    }
  }

  /*Submiting a clone page form  */
  onSubmit(data) {
    if (this.cloneForm.invalid) {
      this.submitted = true;
    } else {
      this.spinner.show();
      
      let draft_html = $('#message-modal #draft_html').val();
      this.cloneForm.controls['draft_html'].setValue(draft_html);
      let publish_html = $('#message-modal #publish_html').val();
      this.cloneForm.controls['publish_html'].setValue(publish_html);
      
      let formData = new FormData()
      formData.append('savetype', this.savetype)
      for (let d in this.cloneForm.value) {
        formData.append(d, this.cloneForm.value[d])
      }
      formData.append('file', this.file);
      return this.http.post(AppSettings.API_ENDPOINT + '/add', formData).subscribe((response: any) => {
        this.spinner.hide();
        if (response.status == 200) {
          $("#message-modal").modal("hide");
          this.getList();
          this.notifier.notify('success', 'Added successfully');
        }
        else {
          this.notifier.notify('error', response.message);
        }
      });
    }
  };

   /*Delete a particular page */
  tedelete() {
    this.http.post(AppSettings.API_ENDPOINT + "deletepage", { "id": this.id }).subscribe((res: any) => {
      if (res.status == 200) {
        $("#delete-modal").modal("hide");
        this.getList();
        this.notifier.notify('success', 'Deleted successfully');
      } else {
        this.notifier.notify('error', 'Something went wrong');
      }
    })
  }

   /*Open modal for delete a page*/
  delete(id) {
    this.id = id,
    $("#delete-modal").modal("show");
  }

   /*Set a vaue for clone page of page*/
  clone(data) {
    let ds = data
    this.cloneForm.controls['pagename'].setValue(ds.pagename)
    this.cloneForm.controls['title'].setValue(ds.title)
    this.cloneForm.controls['subtitle'].setValue(ds.subtitle)
    this.cloneForm.controls['description'].setValue(ds.description)
    this.cloneForm.controls['metatitle'].setValue(ds.metatitle)
    this.cloneForm.controls['metadescription'].setValue(ds.metadescription)
    this.cloneForm.controls['meta_keywords'].setValue(ds.meta_keywords)
    this.cloneForm.controls['draft_html'].setValue(ds.draft_html)
    this.cloneForm.controls['publish_html'].setValue(ds.publish_html)
    this.cloneForm.controls['template'].setValue(ds.template)
    $('#message-modal').modal('show')
  }

  savehtml(type) {
    this.savetype = type;
    if(type == 'Publish' && $('#message-modal #publish_html').val() == ''){
      this.cloneForm.controls['publish_html'].setValue($('#message-modal #draft_html').val());
    }
  }

  isOS() {
    return navigator.userAgent.match(/ipad|iphone/i);
  }

   /*Copy url to clipboard */
  copyTextToClipboard(text) {
    let textArea: any = document.createElement("textarea");
    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.left = 0;
    textArea.style.width = '2em';
    textArea.style.height = '2em';
    textArea.style.padding = 0;
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';
    textArea.style.background = 'transparent';
    textArea.value = text;
    document.body.appendChild(textArea);
    let range: any;
    let selection: any;
    if (this.isOS()) {
      range = document.createRange();
      range.selectNodeContents(textArea);
      selection = window.getSelection();
      selection.removeAllRanges();
      selection.addRange(range);
      textArea.setSelectionRange(0, 999999);
    } else {
      textArea.select();
    }
    try {
      var successful = document.execCommand('copy');
      var msg = successful ? 'successful' : 'unsuccessful';
      this.notifier.notify('success', 'Copied to clipboard');
    } catch (err) {
      this.notifier.notify('error', 'Oops, unable to copy')
    }
    document.body.removeChild(textArea);
  }
}

