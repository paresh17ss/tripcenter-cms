import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageListComponent } from '../page-list/page-list.component';
import { PagesComponent } from '../pages/pages.component';
import { EditpageComponent } from '../editpage/editpage.component';
import { RolemanagementComponent } from '../rolemanagement/rolemanagement.component';
import { BlogsComponent } from '../blogs/blogs.component';
import { BlogCommentComponent } from '../blogcommentlist/blog_comment.component';
import { BlogpageComponent } from '../blogpage/blogpage.component';
import { CategoriesComponent } from '../categories/categories.component';
import { EditblogpageComponent } from '../editblogpage/editblogpage.component';
import { MediaComponent } from '../media/media.component';
import { UsersComponent } from '../users/users.component';
import { NewsComponent } from '../news/news.component';
import { NewspageComponent } from '../newspage/newspage.component';
import { EditnewspageComponent } from '../editnewspage/editnewspage.component';
import { FooterComponent } from '../footer/footer.component';
import { FooterlistComponent } from '../footerlist/footerlist.component';
import { EditfooterComponent } from '../editfooter/editfooter.component';
import { NewscommentlistComponent } from '../newscommentlist/newscommentlist.component';
import { ContactuslistComponent } from '../contactuslist/contactuslist.component';

const routes: Routes = [
  { path: '', redirectTo: 'pagelist' },
  { path: 'pagelist', component: PageListComponent },
  { path: 'addnewpage/:id', component: PagesComponent },
  { path: 'editpage/:id', component: EditpageComponent },
  { path: 'rolemanagement', component: RolemanagementComponent },
  { path: 'bloglist', component: BlogsComponent },
  { path: 'blogcommentlist', component: BlogCommentComponent },
  { path: 'blogpage', component: BlogpageComponent },
  { path: 'categories', component: CategoriesComponent },
  { path: 'editblogpage/:id', component: EditblogpageComponent },
  { path: 'media', component: MediaComponent },
  { path: 'users', component: UsersComponent },
  { path: 'newslist', component: NewsComponent },
  { path: 'newspage', component: NewspageComponent },
  { path: 'editnewspage/:id', component: EditnewspageComponent },
  { path: 'footer', component: FooterComponent },
  { path: 'footerlist', component: FooterlistComponent },
  { path: 'editfooter/:id', component: EditfooterComponent },
  { path: 'newscommentlist', component: NewscommentlistComponent },
  { path: 'contactuslist', component: ContactuslistComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule {}
