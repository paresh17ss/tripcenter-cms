import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout.routing-module';
import { LayoutComponent } from './layout.component';

import { PageListComponent } from '../page-list/page-list.component';
import { PagesComponent } from '../pages/pages.component';
import { EditpageComponent } from '../editpage/editpage.component';
import { RolemanagementComponent } from '../rolemanagement/rolemanagement.component';
import { BlogsComponent } from '../blogs/blogs.component';
import { BlogCommentComponent } from '../blogcommentlist/blog_comment.component';
import { BlogpageComponent } from '../blogpage/blogpage.component';
import { CategoriesComponent } from '../categories/categories.component';
import { EditblogpageComponent } from '../editblogpage/editblogpage.component';
import { MediaComponent } from '../media/media.component';
import { UsersComponent } from '../users/users.component';
import { NewsComponent } from '../news/news.component';
import { NewspageComponent } from '../newspage/newspage.component';
import { EditnewspageComponent } from '../editnewspage/editnewspage.component';
import { FooterComponent } from '../footer/footer.component';
import { FooterlistComponent } from '../footerlist/footerlist.component';
import { EditfooterComponent } from '../editfooter/editfooter.component';
import { NewscommentlistComponent } from '../newscommentlist/newscommentlist.component';
import { ContactuslistComponent } from '../contactuslist/contactuslist.component';
import { NotifierModule } from 'angular-notifier';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DataTableModule } from 'angular-6-datatable';
import { FilterPipe } from '../filter.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RoleDirective } from '../role.directive';

@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,
    NotifierModule.withConfig({
      theme: 'material',
      position: {
        horizontal: {
          /**
           * Defines the horizontal position on the screen
           * @type {'left' | 'middle' | 'right'}
           */
          position: 'middle',

          /**
           * Defines the horizontal distance to the screen edge (in px)
           * @type {number}
           */
          distance: 12
        },

        vertical: {
          /**
           * Defines the vertical position on the screen
           * @type {'top' | 'bottom'}
           */
          position: 'bottom',

          /**
           * Defines the vertical distance to the screen edge (in px)
           * @type {number}
           */
          distance: 12
        }
      }
    })
  ],
  declarations: [
    LayoutComponent,
    PageListComponent,
    PagesComponent,
    EditpageComponent,
    RolemanagementComponent,
    BlogsComponent,
    BlogCommentComponent,
    BlogpageComponent,
    CategoriesComponent,
    EditblogpageComponent,
    MediaComponent,
    UsersComponent,
    NewsComponent,
    NewspageComponent,
    EditnewspageComponent,
    FooterComponent,
    FooterlistComponent,
    EditfooterComponent,
    NewscommentlistComponent,
    ContactuslistComponent,
    FilterPipe,
    RoleDirective
  ]
})
export class LayoutModule {}
