import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../app-setting';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
declare var $: any;
@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.scss']
})
export class MediaComponent implements OnInit {
  public mediaForm: FormGroup;
  public meditForm: FormGroup = new FormGroup({
    title: new FormControl('', Validators.required),
    image: new FormControl('')

  });
  data: any = [];
  id: any;
  path: any = AppSettings.IMAGE_PATH;
  submitted:boolean=false;
  file: any;
  private readonly notifier: NotifierService;
  
  constructor(private fb:FormBuilder,private http: HttpClient, private spinner: NgxSpinnerService
    , private router: Router, private notifierService: NotifierService) {
    this.notifier = notifierService;
   }

  ngOnInit() {
    this.mediaForm = this.fb.group({
      title:  ['',[Validators.required,Validators.pattern(/\w+( +\w+)*$/)]],
      image: ['',[Validators.required]]
    });
    this.getMediaList();
  }

  /*Get media list*/
  getMediaList()
  {
    this.http.get(AppSettings.API_ENDPOINT + 'medialist').subscribe((res: any) => {
      if (res.status == true) {
        this.data = res.data;
      }
    })
  }

   /*Open modal for a add media*/
  addImage() {
    $('#message-modal').modal('show')
  }

   /*Change event for image file change*/
  onFileChange(event) {
    this.file = event.target.files[0];
  }

   /*Submit a add media form*/
  onSubmit(data) {
    if (this.mediaForm.invalid) {
        this.submitted = true;
    }else{
      this.spinner.show();
      let formData = new FormData()
      formData.append('file', this.file);
      formData.append('title', data.title);
       /*Call api for add media*/
      this.http.post(AppSettings.API_ENDPOINT + '/addmedia', formData).subscribe((res: any) => {
        this.spinner.hide();
        if (res.status == 200) {
          $("#message-modal").modal("hide");
          this.getMediaList();
          this.notifier.notify('success', 'Added successfully');
        } else {
          this.notifier.notify('error', res.message)
        }
      })
    }
  }

  /*show modal for edit or update media*/
  editImage(data) {
    this.id = data.id;
    let title = data.title;
    this.meditForm.controls['title'].setValue(title);
    $('#edit-modal').modal('show')
  }

 /*Delete a media*/
  deletemedia() {
    this.http.post(AppSettings.API_ENDPOINT + "deletemedia", { "id": this.id }).subscribe((res: any) => {
      if (res.status == 200) {
        $('#delete-modal').modal('hide');
        this.getMediaList();
        this.notifier.notify('success', 'Deleted successfully')
      } else {
        this.notifier.notify('error', 'Something went wrong')
      }
    })
  }

   /*Open modal for delete media*/
  delete(id){
    this.id = id,
    $('#delete-modal').modal('show');
  }

   /*Edit or upadte a media*/
  medit(data) {
    let title = data.title;
    let formData = new FormData()
    if (this.file != undefined) {
      formData.append('file', this.file);
    }
    formData.append('id', this.id)
    formData.append('title', title)
    this.http.post(AppSettings.API_ENDPOINT + 'editmedia', formData).subscribe((response: any) => {
      if (response.status == 200) {
        this.notifier.notify('success', 'Updated successfully')
        $('#edit-modal').modal('hide')
        this.getMediaList();
      } else {
        this.notifier.notify('error', response.message)
      }

    })
  }

  isOS() {
    return navigator.userAgent.match(/ipad|iphone/i);
  }

   /*Copy url for clipboard*/
  copyTextToClipboard(text) {
    let textArea: any = document.createElement("textarea");
    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.left = 0;
    textArea.style.width = '2em';
    textArea.style.height = '2em';
    textArea.style.padding = 0;
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';
    textArea.style.background = 'transparent';
    textArea.value = text;
    document.body.appendChild(textArea);
    let range: any;
    let selection: any;
    if (this.isOS()) {
      range = document.createRange();
      range.selectNodeContents(textArea);
      selection = window.getSelection();
      selection.removeAllRanges();
      selection.addRange(range);
      textArea.setSelectionRange(0, 999999);
    } else {
      textArea.select();
    }
    try {
      var successful = document.execCommand('copy');
      var msg = successful ? 'successful' : 'unsuccessful';
      this.notifier.notify('success', 'Copied to clipboard');
    } catch (err) {
      this.notifier.notify('error', 'Oops, unable to copy')
    }
    document.body.removeChild(textArea);
  }
}
