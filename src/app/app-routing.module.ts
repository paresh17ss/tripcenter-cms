import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared';

const appRoutes: Routes = [
  {
    path: '',
    loadChildren: './_Layout/layout.module#LayoutModule',
    canActivate: [AuthGuard]
  },
  { path: 'login', loadChildren: './login/login.module#LoginModule' }
];

export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(
  appRoutes,
  { useHash: false }
);
