import { Component, OnInit, HostListener, EventEmitter } from '@angular/core';
declare const $: any;
import {
  Router,
  NavigationStart,
  ActivatedRoute,
  Event,
  NavigationEnd
} from '@angular/router';
import { AuthenticationService } from './shared/services/AuthenticationService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isShowMenu: boolean;
  url: any;
  start_url: any;
  screenHeight: any;
  suburl: any = '';
  help: any = new EventEmitter();
  AdminData: any;
  constructor(
    public rout: ActivatedRoute,
    public router: Router,
    private authenticationService: AuthenticationService
  ) {
    debugger;
    // this.isShowMenu = this.rout.url !== '/';
    //console.log(this.rout);
    router.events.subscribe((event: Event) => {
      //console.log(event);
      if (event instanceof NavigationEnd) {
        this.isShowMenu = event.url !== '/login';
      }
    });
    let current_child = window.location.href.split('/')[4];
    this.suburl = window.location.href.split('/')[5];
    this.url = current_child;
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        this.url = event.url.split('/')[2];
        this.suburl = event.url.split('/')[3];
      }
    });
  }

  ngOnInit() {
    this.AdminData = JSON.parse(localStorage.getItem('AdminData'));
    //$('#sidebar').toggleClass('active');
    this.getScreenSize();
    const firstChild = this.rout.snapshot;
    $('#sidebar').toggleClass('active');
    $(document).ready(function() {
      $('#sidebarCollapse').on('click', function() {
        $('#sidebar').toggleClass('active');
        $('.overlay').addClass('active');
      });
      $('#dismiss, .overlay').on('click', function() {
        $('#sidebar').addClass('active');
      });
    });
  }

  onClickSidebarcollapse = () => {
    $('#sidebar').toggleClass('active');
    $('.overlay').addClass('active');
  };

  onClickdismiss = () => {
    $('#sidebar').addClass('active');
  };

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.screenHeight = window.innerHeight + 'px';
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/']);
  }
}
