export interface SessionModel {
  _User_ID: number;
  _User_Name: string;
  _User_Email: string;
  _User_Token: string;
  _Session_Expiry: string;
}

export class SessionClass {
  _User_ID: number;
  _User_Name: string
  _User_Email: string;
  _User_Token: string;
  _Session_Expiry: string;
}
