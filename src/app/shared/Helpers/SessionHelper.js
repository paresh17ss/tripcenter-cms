"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AppConsts_1 = require("../modules/AppConsts");
function SetSession(data) {
    data._Session_Expiry = new Date().setMinutes(new Date().getMinutes() + 60).toString();
    localStorage.setItem(AppConsts_1.app_session_name, JSON.stringify(data));
}
exports.SetSession = SetSession;
function GetSession(val) {
    var session = localStorage.getItem(AppConsts_1.app_session_name);
    if (val) {
        var dec = JSON.parse(session);
        return dec[val];
    }
    return JSON.parse(session);
}
exports.GetSession = GetSession;
function isSessionExpire() {
    try {
        var session = localStorage.getItem(AppConsts_1.app_session_name);
        var dec = JSON.parse(session);
        var current = new Date();
        var expre = new Date(parseInt(dec._Session_Expiry));
        var is = expre > current;
        if (!is) {
            RemoveSession();
        }
        return is;
    }
    catch (e) {
        return false;
    }
}
exports.isSessionExpire = isSessionExpire;
function RemoveSession() {
    localStorage.removeItem(AppConsts_1.app_session_name);
}
exports.RemoveSession = RemoveSession;
//# sourceMappingURL=SessionHelper.js.map