import { app_session_name } from '../modules/AppConsts';
import { Injectable } from '@angular/core';
import { ILogin } from '../_Interfaces/interface';
import * as CryptoJS from 'crypto-js';
import { SessionClass } from '../_Models/SessionModel';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SessionHelper {
  secreteKey: string;
  constructor(private router: Router) {
    this.secreteKey = '5sdfEFdf544sdtgtj52vh14jdrtG';
  }

  SetSession = (data: any) => {
    var set = new SessionClass();
    set._User_ID = data.id;
    set._User_Email = data.email;
    set._User_Name = data.fname + ' ' + data.lname;
    set._User_Token = '';
    var str = JSON.stringify(set);
    var enc = this.set(str);
    localStorage.setItem(app_session_name, enc);
  };

  GetSessionUserId = () => {
    var session = this.get();
    return session._User_ID;
  };

  GetSessionToken = () => {
    var session = this.get();
    if (!session) {
      return '';
    }
    return session._User_Token;
  };

  GetSession = (val?: string) => {
    var session = this.get();
    if (val) {
      return session[val];
    }
    return session;
  };

  isSessionExpire = () => {
    debugger;
    try {
      var dec = this.get();
      var is = dec._User_ID && dec._User_ID != ''; //expre > current;
      if (!is) {
        this.RemoveSession();
      }
      return is;
    } catch (e) {
      return false;
    }
  };

  RemoveSession = async () => {
    await localStorage.removeItem(app_session_name);
    this.router.navigate(['/login']);
  };

  set(value) {
    var key = CryptoJS.enc.Utf8.parse(this.secreteKey);
    var iv = CryptoJS.enc.Utf8.parse(this.secreteKey);
    var encrypted = CryptoJS.AES.encrypt(
      CryptoJS.enc.Utf8.parse(value.toString()),
      key,
      {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
      }
    );

    return encrypted.toString();
  }

  //The get method is use for decrypt the value.
  get() {
    var value = localStorage.getItem(app_session_name);
    if (!value) {
      return null;
    }
    try {
      var key = CryptoJS.enc.Utf8.parse(this.secreteKey);
      var iv = CryptoJS.enc.Utf8.parse(this.secreteKey);
      var decrypted = CryptoJS.AES.decrypt(value, key, {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
      });

      var str = decrypted.toString(CryptoJS.enc.Utf8);
      return JSON.parse(str);
    } catch (e) {
      return null;
    }
  }
}
