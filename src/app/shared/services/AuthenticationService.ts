import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SessionHelper } from '../Helpers/SessionHelper';
import { ICommon, ILogin, ISession } from '../_Interfaces/interface';
import { AppSettings } from '../../app-setting';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<ISession>;
  public currentUser: Observable<ISession>;

  constructor(private http: HttpClient, private session: SessionHelper) {
    this.currentUserSubject = new BehaviorSubject<ISession>(
      this.session.GetSession()
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): ISession {
    return this.currentUserSubject.value;
  }

  login(email: string, password: string) {
    return this.http
      .post<any>(AppSettings.API_ENDPOINT + '/loginUser', { email, password })
      .pipe(
        map(user => {
          // login successful if there's a jwt token in the response
          if (user.status) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            this.session.SetSession(user.data);
            this.currentUserSubject.next(this.session.GetSession());
          }
          return user;
        })
      );
  }

  logout() {
    // remove user from local storage to log user out
    this.session.RemoveSession();
    this.currentUserSubject.next(null);
  }
}
