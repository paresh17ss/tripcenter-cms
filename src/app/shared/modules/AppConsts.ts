import { HttpHeaders } from "@angular/common/http";

export const api_endpoint = '/API/';
export const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

export enum UserSession {
  _User_ID= 0,
  _User_Name= 1,
  _User_Email= 2,
  _User_Token = 3,
  _Session_Expiry = 4
}
export const app_session_name = '_Asp.sessionManagement';
