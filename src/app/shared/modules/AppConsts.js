"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("@angular/common/http");
exports.api_endpoint = '/API/';
exports.httpOptions = {
    headers: new http_1.HttpHeaders({
        'Content-Type': 'application/json'
    })
};
var UserSession;
(function (UserSession) {
    UserSession[UserSession["_User_ID"] = 0] = "_User_ID";
    UserSession[UserSession["_User_Name"] = 1] = "_User_Name";
    UserSession[UserSession["_User_Email"] = 2] = "_User_Email";
    UserSession[UserSession["_User_Token"] = 3] = "_User_Token";
    UserSession[UserSession["_Session_Expiry"] = 4] = "_Session_Expiry";
})(UserSession = exports.UserSession || (exports.UserSession = {}));
exports.app_session_name = '_Asp.sessionManagement';
//# sourceMappingURL=AppConsts.js.map