export interface ICommon<T> {
  responseCode: number;
  responseMessage: string;
  data: T;
}

export interface ISession {
  _User_ID: number;
  _User_Name: string;
  _User_Email: string;
  _User_Token: string;
  _Session_Expiry: string;
}


export interface ILogin {
  userId: number;
  userName: string;
  userEmail: string;
  userRole: number;
  token: string;
}

// export interface IUser {
//   idnumber: number;
//   companyId: number;
//   userId: string;
//   userName: string;
//   pswd: string;
//   groupId: number;
//   contactNo: string;
//   emailId: string;
//   isOmsaccess: number;
//   isDcmaccess: number;
//   pipedriveId: string;
//   isActive: number;
//   createdBy: number;
//   createdDate: any;
//   updatedBy: number;
//   updatedDate: any;
// }

// export interface IUserRole {
//   idnumber: number;
//   groupName: string;
//   isActive: boolean;
//   isHardCoded: boolean;
// }

// export interface IChangePassword {
//   idnumber: number;
//   userName: string;
//   Password: string;
//   CPassword: string;
// }

