import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { app_session_name } from '../modules/AppConsts';
import { SessionHelper } from '../Helpers/SessionHelper';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router,private session:SessionHelper) {}

    canActivate() {
      if (this.session.isSessionExpire()) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }
}
