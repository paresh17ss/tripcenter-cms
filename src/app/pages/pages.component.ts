import { Component, Renderer2, OnInit, Inject, OnDestroy } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, } from '@angular/common/http';
import { AppSettings } from '../app-setting';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { NotifierService } from 'angular-notifier';
import { template } from '@angular/core/src/render3';

declare var $: any;
declare var Vvveb: any;

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})

export class PagesComponent implements OnInit, OnDestroy {
  public savedForm: FormGroup = new FormGroup({
    pagename: new FormControl('', [Validators.required, Validators.pattern(/\w+( +\w+)*$/)]),
    title: new FormControl('', [Validators.required, Validators.pattern(/\w+( +\w+)*$/)]),
    subtitle: new FormControl('', [Validators.required, Validators.pattern(/\w+( +\w+)*$/)]),
    description: new FormControl('', [Validators.required, Validators.pattern(/\w+( +\w+)*$/)]),
    bgimage: new FormControl('', [Validators.required, Validators.pattern(/\w+( +\w+)*$/)]),
    metatitle: new FormControl('', [Validators.required, Validators.pattern(/\w+( +\w+)*$/)]),
    metadescription: new FormControl('', [Validators.required, Validators.pattern(/\w+( +\w+)*$/)]),
    meta_keywords: new FormControl('', [Validators.required, Validators.pattern(/\w+( +\w+)*$/)]),
    footer: new FormControl('', [Validators.required, Validators.pattern(/\w+( +\w+)*$/)]),
    html: new FormControl(''),
    draft_html: new FormControl(''),
    publish_html: new FormControl(''),
    template: new FormControl('')
  });

  // form:any;
  footer: any = [];
  templateUrl: any;
  file: any;
  private readonly notifier: NotifierService;
  savetype: any;
  submitted: boolean = false;
  constructor(public notifierService: NotifierService, private router: Router, private _location: Location, private spinner: NgxSpinnerService, private _renderer2: Renderer2, @Inject(DOCUMENT) private _document, private http: HttpClient) {
    this.notifier = notifierService;
  };

  /*Event chage for a image file change*/
  onFileChange(event) {
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];
    }
  }

  savehtml(type) {
    this.savetype = type;
    if(type == 'Publish' && $('#message-modal #publish_html').val() == ''){
      this.savedForm.controls['publish_html'].setValue($('#message-modal #draft_html').val());
    }
  }

  /*Submiting a page save form*/
  onSubmit(data) {
    if (this.savedForm.invalid) {
      this.submitted = true;
    } else {
      this.spinner.show();
      let draft_html = $('#message-modal #draft_html').val();
      this.savedForm.controls['draft_html'].setValue(draft_html);
      let publish_html = $('#message-modal #publish_html').val();
      this.savedForm.controls['publish_html'].setValue(publish_html);
      let formData = new FormData()
      for (let d in this.savedForm.value) {
        formData.append(d, this.savedForm.value[d])
      }
      formData.append('savetype', this.savetype)
      formData.append('file', this.file);
      /* Add new page form */
      return this.http.post(AppSettings.API_ENDPOINT + '/add', formData).subscribe((response: any) => {
        this.spinner.hide();
        if (response.status == 200) {
          $("#message-modal").modal("hide");
          this.router.navigate(['/'])
          this.notifier.notify('success', 'Added successfully');
        } else {
          this.notifier.notify('error', response.message);
        }
      });
    }
  };

  backClicked() {
    this._location.back();
  }

  ngOnInit() {
    /*Get a page listing*/
    this.http.get(AppSettings.API_ENDPOINT + 'footerlist').subscribe((res: any) => {
      this.footer = res.data;
    })
    this.templateUrl = decodeURIComponent(this.router.url.replace("/addnewpage/", ""));
    this.templateUrl = atob(this.templateUrl)
    this.savedForm.controls['template'].setValue(this.templateUrl);
    let s = this._renderer2.createElement('script');
    s.type = `text/html`;
    s.text = `
        {
            "@context":'<label class="header" data-header="{%=key%}" for="header_{%=key%}"><span>&ensp;{%=header%}</span> <div class="header-arrow"></div></label> 
            <input class="header_check" type="checkbox" {% if (typeof expanded !== 'undefined' && expanded == false) { %} {% } else { %}checked="true"{% } %} id="header_{%=key%}"> 
            <div class="section" data-section="{%=key%}"></div>'
            /* your schema.org microdata goes here */
        }
    `;

    this._renderer2.appendChild(this._document.body, s);
    let self = this
    setTimeout(() => {
      $(document).ready(function () {
        //if url has #no-right-panel set one panel demo
        $("#vvveb-builder").addClass("no-right-panel");
        $(".component-properties-tab").show();
        Vvveb.Components.componentPropertiesElement = "#left-panel .component-properties";
        Vvveb.Builder.init('assets/templet/' + self.templateUrl + '.html', function () {
          // 'http://localhost:3000/my-page.html'
          Vvveb.Gui.init();
        });
      });
    }, 3000);
  };

  ngOnDestroy() {
    $('body').removeAttr('style');
  }
}