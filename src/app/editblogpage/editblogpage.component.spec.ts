import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditblogpageComponent } from './editblogpage.component';

describe('EditblogpageComponent', () => {
  let component: EditblogpageComponent;
  let fixture: ComponentFixture<EditblogpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditblogpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditblogpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
