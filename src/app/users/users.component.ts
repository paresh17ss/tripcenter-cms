import { Component, OnInit } from '@angular/core';
import { HttpClient, } from '@angular/common/http';
import { AppSettings } from '../app-setting';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
declare var $: any;
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers: [DatePipe]
})

export class UsersComponent implements OnInit {
  myDate = new Date();
  private readonly notifier: NotifierService;
  constructor(private http: HttpClient, private spinner: NgxSpinnerService
    , private notifierService: NotifierService, private datePipe: DatePipe) { 
      this.notifier = notifierService;
    }
  
  public userForm: FormGroup = new FormGroup({
    fName: new FormControl('', Validators.required),
    lName: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    telPhone: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    confirm_password: new FormControl('', Validators.required),
    role: new FormControl('', Validators.required),

  });
  public editForm: FormGroup = new FormGroup({
    fName: new FormControl('', Validators.required),
    lName: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    telPhone: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    confirm_password: new FormControl('', Validators.required),
    role: new FormControl('', Validators.required),
  });
  id: any;
  role: any = [];
  data: any = [];
  rolename: any;
  
  ngOnInit() {
     this.getUserListing();
     this.getRoleListing();
  }
  
  /*Getting a userlisting*/
  getUserListing()
  {
    this.http.get(AppSettings.API_ENDPOINT + 'userlisting').subscribe((res: any) => {
      if (res.status == 200) {
        this.data = res.data;
        this.spinner.hide()
      } else {
        this.notifier.notify('error', 'Something went wrong');
      }
    })
  }

  /*Getting a rolelisting*/
  getRoleListing()
  {
    this.http.get(AppSettings.API_ENDPOINT + 'rolelist').subscribe((res: any) => {
      this.role = res.data;
    })
  }

  /*Open modal for add new user*/
  addusers(data) {
    $('#message-modal').modal('show')
  }

  /*Submit a adduser form*/
  onSubmit(data) {
    let formData = new FormData()
    for (let d in this.userForm.value) {
      formData.append(d, this.userForm.value[d])
    }
    let dt = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
    formData.append('dateCreated', dt)
    /*Add new user api*/
    return this.http.post(AppSettings.API_ENDPOINT + 'adduser', formData).subscribe((res: any) => {
      if (res.status == 200) {
        $("#message-modal").modal("hide");
        this.getUserListing();
        this.notifier.notify('success', 'Added successfully');
      } else {
        this.notifier.notify('error', res.message)
      }
    })
  }

  /*Open modal for edit user*/
  editmodal(data) {
    this.id = data.id;
    this.editForm.controls['fName'].setValue(data.fName);
    this.editForm.controls['lName'].setValue(data.lName);
    this.editForm.controls['email'].setValue(data.email);
    this.editForm.controls['telPhone'].setValue(data.telPhone);
    this.editForm.controls['password'].setValue(data.password);
    this.editForm.controls['confirm_password'].setValue(data.password);
    this.editForm.controls['role'].setValue(data.role_id);
    $('#edit-modal').modal('show')
  }

   /*Edit user*/
  edituser(data) {
    this.http.post(AppSettings.API_ENDPOINT + 'edituser', { id: this.id, ...data }).subscribe((res: any) => {
      if (res.status == 200) {
        $('#edit-modal').modal('hide')
        this.getUserListing();
        this.notifier.notify('success', 'Updated successfully');
      } else {
        this.notifier.notify('error', res.message)
      }
    })
  }

  /*open modal for delete user*/
  delete(id) {
    this.id = id,
    $('#delete-modal').modal('show');
  }

 /*Delete a user*/
  deleteuser() {
    this.http.post(AppSettings.API_ENDPOINT + 'deleteuser', { id: this.id }).subscribe((res: any) => {
      if(res.status == 200){
        $('#delete-modal').modal('hide');
        this.getUserListing();
        this.notifier.notify('success', 'Deleted successfully');
      }else{
        this.notifier.notify('error','Something went wrong')
      }
    })
  }
}
