import { Directive,Renderer2,ElementRef, HostListener } from '@angular/core';
declare var $:any;
@Directive({
  selector: '[appClickdirective]'
})
export class ClickdirectiveDirective {

  constructor() { 
   
  }
  @HostListener('click')  
  onclick(){
    $('#sidebar').toggleClass('active');
    // close dropdowns
    $('.collapse.in').toggleClass('in');
    // and also adjust aria-expanded attributes we use for the open/closed arrows
    // in our CSS
    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
  }


}
