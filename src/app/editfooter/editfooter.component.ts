import { Component, Renderer2, OnInit, Inject, OnDestroy } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, } from '@angular/common/http';
import { AppSettings } from '../app-setting';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { NotifierService } from 'angular-notifier';
import { template } from '@angular/core/src/render3';
declare var $: any;
declare var Vvveb: any;

@Component({
  selector: 'app-editfooter',
  templateUrl: './editfooter.component.html',
  styleUrls: ['./editfooter.component.scss']
})
export class EditfooterComponent implements OnInit {

  public savedfooterForm: FormGroup = new FormGroup({
    footername: new FormControl('', Validators.required),
    html: new FormControl('')
  });

  // form:any;
  templateUrl: any;
  file: any;
  private readonly notifier: NotifierService;
  templetUrl: any;
  footername: any;
  id: any;
  constructor(public notifierService: NotifierService, private router: Router, private _location: Location, private spinner: NgxSpinnerService, private _renderer2: Renderer2, @Inject(DOCUMENT) private _document, private http: HttpClient) {
    this.notifier = notifierService;
  };
  
 /*Change event for image file*/
  onFileChange(event) {
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];
    }
  }

   /* Submit updatefooter form*/
  onSubmit(data) {
    this.spinner.show();
    let html = $('#message-modal textarea').val();
    this.savedfooterForm.controls['html'].setValue(html);
    let formData = new FormData()
    formData.append('id', this.id);
    for (let d in this.savedfooterForm.value) {

      formData.append(d, this.savedfooterForm.value[d])
    }

   /*Api for update footer*/
    return this.http.post(AppSettings.API_ENDPOINT + '/editfooter', formData).subscribe((response: any) => {
      this.spinner.hide();
      if (response.status == 200) {
        $("#message-modal").modal("hide");
        this.router.navigate(['footerlist'])
        this.notifier.notify('success', 'Updated successfully');
      }
      else {
        this.notifier.notify('error', response.message);
      }
    });
  };

  backClicked() {
    this._location.back();
  }

  ngOnInit() {
    this.templateUrl = decodeURIComponent(this.router.url.replace("/editfooter/", ""));
    this.templateUrl = atob(this.templateUrl)
    this.id = this.templateUrl;
     /*Get footer for render*/
    this.http.post(AppSettings.API_ENDPOINT + 'getfooter', { id: this.id }).subscribe((res: any) => {
      if (res.status == 200) {
        let ds = res.data
        this.savedfooterForm.controls['footername'].setValue(ds.footer_name)
        this.savedfooterForm.controls['html'].setValue(ds.html)
        this.id = ds.id;
        this.footername = ds.footer_name
       let url = this.footername.toLowerCase().replace(" ", "-")
        let s = this._renderer2.createElement('script');
        s.type = `text/html`;
        s.text = `
        {
            "@context":'<label class="header" data-header="{%=key%}" for="header_{%=key%}"><span>&ensp;{%=header%}</span> <div class="header-arrow"></div></label> 
            <input class="header_check" type="checkbox" {% if (typeof expanded !== 'undefined' && expanded == false) { %} {% } else { %}checked="true"{% } %} id="header_{%=key%}"> 
            <div class="section" data-section="{%=key%}"></div>'
            /* your schema.org microdata goes here */
        }
    `;

        this._renderer2.appendChild(this._document.body, s);
        let self = this
        setTimeout(() => {
          $(document).ready(function () {
            //if url has #no-right-panel set one panel demo

            $("#vvveb-builder").addClass("no-right-panel");
            $(".component-properties-tab").show();
            Vvveb.Components.componentPropertiesElement = "#left-panel .component-properties";
            Vvveb.Builder.init('assets/footer/'+ url +'.html', function () {
              // 'http://localhost:3000/my-page.html'
              Vvveb.Gui.init();
            });
          });
        }, 3000);
      }
    });
  };

   /*Destroy body*/
  ngOnDestroy() {
    $('body').removeAttr('style');
  }
}
