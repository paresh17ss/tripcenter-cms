import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { AppSettings } from '../app-setting';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;
import { HttpClient, } from '@angular/common/http';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.scss']
})
export class BlogsComponent implements OnInit {
  private readonly notifier: NotifierService;
  selectedvalue: string = 'city';
  data: any = [];
  id: any;
  deleteId: number = 0;
  path: any = AppSettings.IMAGE_PATH;
  constructor(private notifierService: NotifierService, private http: HttpClient, private router: Router, private spinner: NgxSpinnerService) {
    this.notifier = notifierService;
  }

  ngOnInit() {
   this.getList();
  }

  /*Listing of blogpages */
  getList()
  {
    this.http.get(AppSettings.API_ENDPOINT + "/bloglist").subscribe((res: any) => {
      if (res.status == 200) {
        this.spinner.hide()
        this.data = res.data;
      } else {
        this.notifier.notify('error', 'Something went wrong');
      }
    });
  }

 /*Go for create a new blogpage */
  newpage() {
    this.router.navigate(['blogpage/']);
  }

  /*open edit blogpage */
  editblog(val) {
    this.id = val;
    let data = btoa(val)
    this.router.navigate(['editblogpage/' + data]);
  }

  /*delete the blog page */
  deleteblog() {
    this.http.post(AppSettings.API_ENDPOINT + "/deleteblog", { "id": this.id }).subscribe((res: any) => {
      if (res.status == 200) {
        this.getList();
        this.notifier.notify('success', 'Deleted successfully');
      } else {
        this.notifier.notify('error', 'Something went wrong')
      }
    })
  }

  /* open modal of delete  */
  delete(id) {
    this.id = id;
    $('#delete-modal').modal('show')
  }

  selectChangeHandler(event: any) {
    //update the ui
    this.selectedvalue = event.target.value;
  }

}
