import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { AppSettings } from "../app-setting";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { NotifierService } from "angular-notifier";
declare var $: any;
@Component({
  selector: "app-blog-comment",
  templateUrl: "./blog_comment.component.html",
  styleUrls: ["./blog_comment.component.scss"]
})
export class BlogCommentComponent implements OnInit {
  data: any = [];
  id: any;
  commentBlogName: string;
  txtReply: string = "";
  tmpItem: any;
  private readonly notifier: NotifierService;
  public editForm: FormGroup = new FormGroup({
    id: new FormControl(0),
    blog_id: new FormControl(0),
    name: new FormControl("", Validators.required),
    message: new FormControl("", Validators.required),
    mail: new FormControl("", Validators.required),
    website: new FormControl("", Validators.required)
  });
  constructor(
    private http: HttpClient,
    private spinner: NgxSpinnerService,
    private router: Router,
    private notifierService: NotifierService
  ) {
    this.notifier = notifierService;
  }

  ngOnInit() {
    this.getCommentList();
  }

  getCommentList = () => {
    this.http
      .get(AppSettings.API_ENDPOINT + "getcommentlist")
      .subscribe((res: any) => {
        if (res.status == true) {
          this.data = res.data;
        }
      });
  };

  cdelete() {
    this.http
      .post(AppSettings.API_ENDPOINT + "delete-comment", { id: this.id })
      .subscribe((res: any) => {
        if (res.status) {
          this.getCommentList();
          this.notifier.notify("success", "Deleted successfully");
        } else {
          this.notifier.notify("error", "Something went wrong");
        }
      });
  }

  delete(id) {
    this.id = id;
    $("#delete-modal").modal("show");
  }

  onApprove = (item: any) => {
    item.status = item.status == 0 ? 1 : 0;
    this.http
      .put(AppSettings.API_ENDPOINT + "update-comment", item)
      .subscribe((res: any) => {
        if (res.status) {
          this.notifier.notify("success", "Updated successfully");
        } else {
          this.notifier.notify("error", "Something went wrong");
        }
      });
  };

  editsave(data: any) {
    this.http
      .put(AppSettings.API_ENDPOINT + "update-comment", { ...data })
      .subscribe((res: any) => {
        if (res.status) {
          $("#edit-modal").modal("hide");
          this.getCommentList();
          this.notifier.notify("success", "Updated successfully");
        } else {
          this.notifier.notify("error", "Something went wrong");
        }
      });
  }
  onSubmitComment = () => {
    this.tmpItem.message = this.txtReply;
    this.tmpItem.isReply = true;
    this.tmpItem.belong_to_comment = this.tmpItem.id;
    this.tmpItem.name = "Admin";
    this.tmpItem.mail = "admin@tripcenter.net";
    this.tmpItem.website = "tripcenter.net";
    delete this.tmpItem.id;
    delete this.tmpItem.createdAt;
    delete this.tmpItem.updatedAt;
    this.fnAddComment(this.tmpItem).subscribe((res: any) => {
      if (res.status) {
        this.getCommentList();
        this.tmpItem = null;
        this.txtReply = "";
      } else {
        this.notifier.notify("error", "Something went wrong");
      }
    });
  };

  onReply = (item: any) => {
    this.txtReply = "";
    this.tmpItem = item;
    $("#reply-modal").modal("show");
  };

  fnAddComment = (data: any) => {
    return this.http.post(AppSettings.API_ENDPOINT + "add-blog-comment", data);
  };

  onEditComment(item: any) {
    this.editForm.reset();
    this.editForm.controls["id"].setValue(item.id);
    this.editForm.controls["blog_id"].setValue(item.blog_id);
    this.editForm.controls["name"].setValue(item.name);
    this.editForm.controls["message"].setValue(item.message);
    this.editForm.controls["mail"].setValue(item.mail);
    this.editForm.controls["website"].setValue(item.website);
    $("#edit-modal").modal("show");
  }
}
