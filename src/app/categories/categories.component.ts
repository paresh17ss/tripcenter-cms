import { Component, OnInit } from '@angular/core';
import{ FormGroup, FormControl, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {AppSettings} from '../app-setting';
import {NgxSpinnerService} from 'ngx-spinner';
import { Router } from '@angular/router';
import {NotifierService} from 'angular-notifier';
declare var $: any;
@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  data: any = [];
  id:any;
  submitted: boolean = false;
  private readonly notifier: NotifierService;

  public savedForm: FormGroup = new FormGroup({
     categoryname: new FormControl('', Validators.required),
  });

  public editForm: FormGroup = new FormGroup({
    categoryname: new FormControl('', Validators.required),
  })
  constructor(private http: HttpClient,private spinner:NgxSpinnerService,private router: Router,private notifierService:NotifierService) { 
    this.notifier = notifierService;
  }

  ngOnInit() {
     this.getCategoryList();
  }

  /*Listing of Categories */
  getCategoryList()
  {
    this.http.get(AppSettings.API_ENDPOINT + 'categorylist').subscribe((res:any)=>{
      if(res.status == true){
          this.data = res.data;
      }
    })  
  }

  /* Open a modal for add new category */
  addcategories(){
    $('#message-modal').modal('show')
  }

  /*Submiting a addcategoriesform */
  onSubmit(data){
    if (this.savedForm.invalid) {
      this.submitted = true;
    } else {
      let formData = new FormData()
      for (let d in this.savedForm.value) {
        formData.append(d, this.savedForm.value[d])
      }
      /*Add new categorie*/
      return this.http.post(AppSettings.API_ENDPOINT + '/addcategorie', formData).subscribe((response: any) => {
        this.spinner.hide();
        if (response.status == true) {
          $("#message-modal").modal("hide");
          this.getCategoryList();
          this.notifier.notify( 'success', 'Added successfully' );
        }
        else {
          this.notifier.notify( 'error', response.message );
        }
      });
    }
  }

  /* Open modal for edit or update a category */
  editcategories(data){
    this.id = data.id;
    let categoryname = data.name;
    this.editForm.controls['categoryname'].setValue(categoryname);
    $('#edit-modal').modal('show')
  }
  
  /* Edit or update a category */
  editsave(data){
    this.http.post(AppSettings.API_ENDPOINT + 'editcategory',{id:this.id,...data}).subscribe((res:any)=>{
      if(res.status == 200){
        $('#edit-modal').modal('hide')
        this.getCategoryList();
        this.notifier.notify( 'success', 'Updated successfully' );
      }else{
        this.notifier.notify('error', res.message);
      }
    })
  }

  /* Open modal for delete a category */
  delete(id){
    this.id = id
    $('#delete-modal').modal('show')
  }

  /* Delete a category */
  cdelete() {
    this.http.post(AppSettings.API_ENDPOINT + "deletecategory", { "id": this.id }).subscribe((res: any) => {
      if (res.status == 200) {
        $('#delete-modal').modal('hide');
        this.getCategoryList();
        this.notifier.notify( 'success', 'Deleted successfully' );
      } else {
        this.notifier.notify( 'error', 'Something went wrong' );
      }
    })
  }
}
