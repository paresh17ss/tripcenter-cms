import { Component, Renderer2, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { HttpClient, } from '@angular/common/http';
import { AppSettings } from '../app-setting';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { NotifierService } from 'angular-notifier';
import { element } from '@angular/core/src/render3';
import * as moment from 'moment'
declare var $: any;
// import * as $ from 'jquery';
declare var tinymce: any;

@Component({
  selector: 'app-editnewspage',
  templateUrl: './editnewspage.component.html',
  styleUrls: ['./editnewspage.component.scss']
})
export class EditnewspageComponent implements OnInit {
  public statusResult: boolean = false;
  public editForm: FormGroup = new FormGroup({
    newsname: new FormControl('', [Validators.required,Validators.pattern(/\w+( +\w+)*$/)]),
    html: new FormControl(''),
    publish: new FormControl(1),
    categorie: new FormArray([],),
    status: new FormControl('Draft', Validators.required),
    visibility: new FormControl('Public'),
    publish_date: new FormControl('',),
    publish_on: new FormControl('Immediately',),
    tag: new FormControl('',[Validators.pattern(/\w+( +\w+)*$/)]),
    featureimage: new FormControl('',[Validators.required,Validators.pattern(/\w+( +\w+)*$/)]),
    meta_title: new FormControl('',[Validators.required,Validators.pattern(/\w+( +\w+)*$/)]),
    meta_description: new FormControl('',[Validators.required,Validators.pattern(/\w+( +\w+)*$/)])
  });

  // form:any;
  templetUrl: any;
  tag: any;
  id: any;
  file: any;
  private readonly notifier: NotifierService;
  tags: any = [];
  data: any = [];
  newsname: any;
  selectedvalue: any;
  submitted:boolean=false;

  constructor(public notifierService: NotifierService, private router: Router, private _location: Location, private spinner: NgxSpinnerService, private _renderer2: Renderer2, @Inject(DOCUMENT) private _document, private http: HttpClient) {
    this.notifier = notifierService;
  };
  
 /*Fetatured image file change event*/
  onFileChange(event) {
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];
    }
  }

   /*Change event for select a status*/
  selectChangeHandler(event: any) {
    //update the ui
    this.statusResult = event.target.value == 'Publish';
    this.selectedvalue = event.target.value;
  }

     /*Submit editnews form*/
  onSubmit(data) {
    if (this.editForm.invalid) {
      this.submitted = true;
    } 
    else 
    {
      let html = tinymce.get('full-featured').getContent()
      var truevalues = this.editForm.value.categorie.filter(function(element) {
        return (element == true);
      })

      if(truevalues == '') {
        this.notifier.notify('error','Category is required');
      } 
      else if(this.tags.length == 0) {
          this.notifier.notify('error','Tag is required');
      } 
      else if(html.indexOf('<p>') == -1) {
        this.notifier.notify('error','News detail is required');
      }
      else 
      {
        this.spinner.show();
        this.editForm.controls['html'].setValue(html);
        this.editForm.controls['tag'].setValue(this.tags.join(','));
        let now = new Date();
        if (this.editForm.value.publish_on == 'Immediately') {
          this.editForm.controls['publish_date'].setValue(now);
        }
        let formData = new FormData()
        for (let d in this.editForm.value) {
          formData.append(d, this.editForm.value[d])
        }
        formData.append('id', this.id);
        let d = [];
        this.data.forEach((element, index) => {
          if (this.editForm.value.categorie[index]) {
            d.push(element.id)
          }
        });
        formData.append('categories', d.join(','))
        if (this.file != undefined) {
          formData.append('file', this.file);
        }

        /*Api of  update newpage  */
        return this.http.post(AppSettings.API_ENDPOINT + '/updatenews', formData).subscribe((response: any) => {
          this.spinner.hide();
          if (response.status == 200) {
            this.notifier.notify('success', 'Updated successfully')
            this.router.navigate(['newslist'])
          }  else {
            this.notifier.notify('error', response.message);
          }
        });
      }
    }
  };

   /*Add in tags chip*/
  taged(val) {
    if(val != '') {
      this.tag = val;
      this.tags.push(this.tag) 
    } 
  }

  backClicked() {
    this._location.back();
  }

  ngOnInit() {
    this.templetUrl = decodeURIComponent(this.router.url.replace("/editnewspage/", ""));
    this.templetUrl = atob(this.templetUrl)
     /*get a news pages */
    this.http.post(AppSettings.API_ENDPOINT + 'getnews', { id: this.templetUrl }).subscribe((res: any) => {
      if (res.status == 200) {
        let data = res.data;
        this.id = data.id;
        this.editForm.controls['newsname'].setValue(data.postname)
         /*Get category list*/
        this.http.get(AppSettings.API_ENDPOINT + 'categorylist').subscribe((response: any) => {
          if (response.status == true) {
            let arra = new FormArray([]);
            let cate = data.categories.split(',')
            response.data.forEach((element, index) => {
              arra.push(new FormControl(cate.indexOf(element.id.toString()) != -1))
            });
            this.editForm.setControl('categorie', arra)
            this.data = response.data;
          }
        })
        let publish_date = moment(data.publish_date).format("MM DD YY");
        this.editForm.controls['publish_date'].setValue(publish_date);
        this.editForm.controls['status'].setValue(data.status.trim());
        this.statusResult = data.status.trim() == 'Publish';
        this.tags = data.tag.split(',');
        this.editForm.controls['html'].setValue(data.drafthtml);
        this.editForm.controls['visibility'].setValue(data.visibility.trim());
        if(null != data.publish_on)
          this.editForm.controls['publish_on'].setValue(data.publish_on.trim());
        this.editForm.controls['meta_title'].setValue(data.meta_title);
        this.editForm.controls['meta_description'].setValue(data.meta_description);
        this.newsname = data.newsname;
        setTimeout(() => {
           /*render particular newspage*/
          tinymce.init({
            selector: 'textarea#full-featured',
            plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount tinymcespellchecker a11ychecker imagetools textpattern help formatpainter permanentpen pageembed tinycomments mentions linkchecker',
            toolbar: 'formatselect | bold italic strikethrough forecolor backcolor permanentpen formatpainter | link image media pageembed | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat | addcomment',
            image_advtab: true,
            html: "",
            content_css: [
              '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
              '//www.tiny.cloud/css/codepen.min.css'
            ],
            link_list: [
              { title: 'My page 1', value: 'http://www.tinymce.com' },
              { title: 'My page 2', value: 'http://www.moxiecode.com' }
            ],
            image_list: [
              { title: 'My page 1', value: 'http://www.tinymce.com' },
              { title: 'My page 2', value: 'http://www.moxiecode.com' }
            ],
            image_class_list: [
              { title: 'None', value: '' },
              { title: 'Some class', value: 'class-name' }
            ],
            importcss_append: true,
            height: 400,
            file_picker_callback: function (callback, value, meta) {
              /* Provide file and text for the link dialog */
              if (meta.filetype === 'file') {
                callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
              }

              /* Provide image and alt text for the image dialog */
              if (meta.filetype === 'image') {
                callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
              }

              /* Provide alternative source and posted for the media dialog */
              if (meta.filetype === 'media') {
                callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
              }
            },
            templates: [
              { title: 'Some title 1', description: 'Some desc 1', content: 'My content' },
              { title: 'Some title 2', description: 'Some desc 2', content: '<div class="mceTmpl"><span class="cdate">cdate</span><span class="mdate">mdate</span>My content2</div>' }
            ],
            template_cdate_format: '[CDATE: %m/%d/%Y : %H:%M:%S]',
            template_mdate_format: '[MDATE: %m/%d/%Y : %H:%M:%S]',
            image_caption: true,
            spellchecker_dialog: true,
            spellchecker_whitelist: ['Ephox', 'Moxiecode'],
            tinycomments_mode: 'embedded',
            content_style: '.mce-annotation { background: #fff0b7; } .tc-active-annotation {background: #ffe168; color: black; }'
          });
        });
      }
      tinymce.remove('textarea#full-featured');
    })
  };
}
