import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditnewspageComponent } from './editnewspage.component';

describe('EditnewspageComponent', () => {
  let component: EditnewspageComponent;
  let fixture: ComponentFixture<EditnewspageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditnewspageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditnewspageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
