import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import{ FormGroup, FormControl, Validators} from '@angular/forms';
import { AppSettings } from "../app-setting";
import {NotifierService} from 'angular-notifier';
declare var $: any;

@Component({
  selector: 'app-contactuslist',
  templateUrl: './contactuslist.component.html',
  styleUrls: ['./contactuslist.component.scss']
})

export class ContactuslistComponent implements OnInit {
  data: any = [];
  id:any;
  submitted: boolean = false;
  subject: any = [];
  interestedIn: any = [];
  booking: any = [];
  public result: boolean = false;
  private readonly notifier: NotifierService;

  public editForm: FormGroup = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    phoneNo: new FormControl('', Validators.required),
    subject: new FormControl('', Validators.required),
    interestedIn: new FormControl(''),
    booking: new FormControl(''),
    message: new FormControl('', Validators.required),
  })

  constructor(private http: HttpClient, private notifierService:NotifierService) { 
    this.notifier = notifierService;
  }

  ngOnInit() {
    this.getContactUsList();
      this.subject = ['Customer Care', 'Supplier Support', 'Tech Team', 'Press Office', 'Privacy Issue', 'Something Else'];
      this.interestedIn = ['API integration', 'Arranging daytrips and multi-day tours', 'Booking private transport requirements', 'Group tour organiser and often need transport', 'Outbound individual and/ or group tour operator', 'TripCenter Direct (platform)', 'Travel agent or a corporate and need private transfers'];
      this.booking = ['1-10', '11-50', '51-100', '101-500', '501-1,000', '1,000-5,000', 'Over 5,000'];
  }

  /* Get Contact-us Listing */
  getContactUsList = () => {
    this.http
      .get(AppSettings.API_ENDPOINT + "getContactUsList")
      .subscribe((res: any) => {
        if (res.status == true) {
          this.data = res.data;
        }
      });
  };

  /* Delete contact-us */
  cdelete() {
    this.http.post(AppSettings.API_ENDPOINT + "deleteContactUS", { "id": this.id }).subscribe((res: any) => {
      if (res.status == 200) {
        $('#delete-modal').modal('hide');
        this.getContactUsList();
        this.notifier.notify( 'success', 'Deleted successfully' );
      } else {
        this.notifier.notify( 'error', 'Something went wrong' );
      }
    })
  }

  /* Open modal for delete a contact-us */
  delete(id){
    this.id = id;
    $('#delete-modal').modal('show');
  }

  /* Edit or update a contact-us */
  editsave(data){
    if (this.editForm.invalid) {
      this.submitted = true;
    } else {
      if(data.subject == 'Something Else' && data.interestedIn == null)
      {
        this.notifier.notify('error','Interested In is required');
      }
      else if(data.subject == 'Something Else' && data.booking == null)
      {
        this.notifier.notify('error','Booking in a year is required');
      }
      else
      {
        data.interestedIn = data.subject == 'Something Else' ? data.interestedIn : '';
        data.booking = data.subject == 'Something Else' ? data.booking : '';
        this.http.post(AppSettings.API_ENDPOINT + 'editContactUS',{id:this.id,...data}).subscribe((res:any)=>{
          if(res.status == 200){
            $('#edit-modal').modal('hide')
            this.notifier.notify( 'success', 'Updated successfully' );
            this.http.get(AppSettings.API_ENDPOINT + 'getContactUsList').subscribe((res:any)=>{
              if(res.status == true){
                  this.data = res.data;
              }
            })  
          } else {
            this.notifier.notify('error','Something went wrong');
          }
        })
      }
    }
  }

  /* Open modal for edit or update a contact-us */
  editContactUs(data){
    this.id = data.id;
    this.editForm.controls['firstName'].setValue(data.firstName);
    this.editForm.controls['lastName'].setValue(data.lastName);
    this.editForm.controls['email'].setValue(data.email);
    this.editForm.controls['phoneNo'].setValue(data.phoneNo);
    this.editForm.controls['subject'].setValue(data.subject);
    this.result = data.subject == 'Something Else';
    this.editForm.controls['interestedIn'].setValue(data.interestedIn);
    this.editForm.controls['booking'].setValue(data.booking);
    this.editForm.controls['message'].setValue(data.message);
    $('#edit-modal').modal('show')
  }

  selectChangeHandler(event: any) {
    this.result = event.target.value == 'Something Else';
  }
}


