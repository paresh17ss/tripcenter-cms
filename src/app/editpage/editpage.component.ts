
import { Component, Renderer2, OnInit, Inject, OnDestroy } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router'
import { AppSettings } from '../app-setting';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
declare var $: any;
// import * as $ from 'jquery';
declare var Vvveb: any;
@Component({
  selector: 'app-editpage',
  templateUrl: './editpage.component.html',
  styleUrls: ['./editpage.component.scss']
})
export class EditpageComponent implements OnInit, OnDestroy {
  id: any;
  pagename: any;
  public savedForm: FormGroup = new FormGroup({
    pagename: new FormControl('', Validators.required),
    title: new FormControl('', Validators.required),
    subtitle: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    bgimage: new FormControl(''),
    metatitle: new FormControl('', Validators.required),
    metadescription: new FormControl('', Validators.required),
    meta_keywords: new FormControl('', Validators.required),
    draft_html: new FormControl(''),
    publish_html: new FormControl(''),
    template: new FormControl('')
  });

  private sub: any;
  pageurl: any;
  private readonly notifier: NotifierService;
  templetUrl: any;
  file: any;
  savetype: any;
  submitted: boolean = false;
  constructor(public notifierService: NotifierService, private spinner: NgxSpinnerService, private location: Location, private router: Router, private dom: DomSanitizer, private route: ActivatedRoute, private _renderer2: Renderer2, @Inject(DOCUMENT) private _document, private http: HttpClient) {
    this.notifier = notifierService;
  };

   /*Submit a updateform*/
  onSubmit(data) {
    if (this.savedForm.invalid) {
      this.submitted = true;
    } else {
      this.spinner.show();
      
      let draft_html = $('#message-modal #draft_html').val();
      this.savedForm.controls['draft_html'].setValue(draft_html);
      let publish_html = $('#message-modal #publish_html').val();
      this.savedForm.controls['publish_html'].setValue(publish_html);
      let formData = new FormData()
      formData.append('savetype', this.savetype)
      for (let d in this.savedForm.value) {
        formData.append(d, this.savedForm.value[d])
      }
      formData.append('id', this.id);
      if (this.file != undefined) {
        formData.append('file', this.file);
      }
      /*Api for update or add page */
      return this.http.post(AppSettings.API_ENDPOINT + '/updatepage', formData).subscribe((response: any) => {
        this.spinner.hide();
        if (response.status == 200) {
          $("#message-modal").modal("hide");
          this.router.navigate(['/'])
          this.notifier.notify('success', 'Updated successfully')
        } 
        else {
          this.notifier.notify('error', response.message)
        }
      });
    }
  };

   /*Background image file change event*/
  onFileChange(event) {
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];
    }
  }

  savehtml(type) {
    this.savetype = type;
    if(type == 'Publish' && $('#message-modal #publish_html').val() == ''){
      this.savedForm.controls['publish_html'].setValue($('#message-modal #draft_html').val());
    }
  }

  backClicked() {
    this.location.back();
  }
  ngOnInit() {
    
    this.templetUrl = decodeURIComponent(this.router.url.replace("/editpage/", ""));
    this.templetUrl = atob(this.templetUrl)
     /*Get the pages and set the value of edit*/
    this.http.post(AppSettings.API_ENDPOINT + 'edit', { id: this.templetUrl }).subscribe((res: any) => {
      if (res.status == 200) {
        let ds = res.data
        this.savedForm.controls['pagename'].setValue(ds.pagename)
        this.savedForm.controls['title'].setValue(ds.title)
        this.savedForm.controls['subtitle'].setValue(ds.subtitle)
        this.savedForm.controls['description'].setValue(ds.description)
        this.savedForm.controls['metatitle'].setValue(ds.metatitle)
        this.savedForm.controls['metadescription'].setValue(ds.metatitle)
        this.savedForm.controls['meta_keywords'].setValue(ds.meta_keywords)
        this.savedForm.controls['draft_html'].setValue(ds.draft_html)
        this.savedForm.controls['publish_html'].setValue(ds.publish_html)
        this.savedForm.controls['template'].setValue(ds.template)
        this.pageurl = ds.url
        this.id = ds.id;
        this.pagename = ds.pagename
        let s = this._renderer2.createElement('script');
        s.type = `text/html`;
        s.text = `
        {
            "@context":'<label class="header" data-header="{%=key%}" for="header_{%=key%}"><span>&ensp;{%=header%}</span> <div class="header-arrow"></div></label> 
            <input class="header_check" type="checkbox" {% if (typeof expanded !== 'undefined' && expanded == false) { %} {% } else { %}checked="true"{% } %} id="header_{%=key%}"> 
            <div class="section" data-section="{%=key%}"></div>'
            /* your schema.org microdata goes here */
        }
    `;
      
        this._renderer2.appendChild(this._document.body, s);

        setTimeout(() => {
          var self = this;
          let filename = self.pageurl + '.html';
          // var url = 'https://tripcenter.azureedge.net/cms/pages/html/'+self.pageurl+'.html'
          $(document).ready(function () {
            //if url has #no-right-panel set one panel demo

            $("#vvveb-builder").addClass("no-right-panel");
            $(".component-properties-tab").show();
            Vvveb.Components.componentPropertiesElement = "#left-panel .component-properties";
               /*Render a page which is edit (path)*/
            Vvveb.Builder.init(AppSettings.HTMLPAGE_URL + filename, function () {
              Vvveb.Gui.init();
            });
          });
        }, 1000);
      }
    })

  };

  ngOnDestroy() {
    $('body').removeAttr('style');
  }

}

