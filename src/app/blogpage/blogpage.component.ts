import { Component, Renderer2, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { HttpClient, } from '@angular/common/http';
import { AppSettings } from '../app-setting';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { NotifierService } from 'angular-notifier';
declare var $: any;
declare var tinymce: any;
@Component({
  selector: 'app-blogpage',
  templateUrl: './blogpage.component.html',
  styleUrls: ['./blogpage.component.scss']
})
export class BlogpageComponent implements OnInit {
  public statusResult: boolean = false;
  public savedForm: FormGroup = new FormGroup({
    blogname: new FormControl('', [Validators.required,Validators.pattern(/\w+( +\w+)*$/)]),
    html: new FormControl(''),
    publish: new FormControl(1),
    categorie: new FormArray([],),
    status: new FormControl('Draft', Validators.required),
    visibility: new FormControl('Public'),
    publish_date: new FormControl('',),
    publish_on: new FormControl('Immediately',),
    tag: new FormControl('',[Validators.required,Validators.pattern(/\w+( +\w+)*$/)]),
    featureimage: new FormControl('',[Validators.required,Validators.pattern(/\w+( +\w+)*$/)]),
    meta_title: new FormControl('',[Validators.required,Validators.pattern(/\w+( +\w+)*$/)]),
    meta_description: new FormControl('',[Validators.required,Validators.pattern(/\w+( +\w+)*$/)]),
  });
  templateUrl: any;
  visibilty: boolean = false;
  selectedvalue: any;
  tag: any;
  file: any;
  tags: any = [];
  data: any = [];
  submitted:boolean=false;
  private readonly notifier: NotifierService;
  constructor(public notifierService: NotifierService, private router: Router, private _location: Location, private spinner: NgxSpinnerService, private _renderer2: Renderer2, @Inject(DOCUMENT) private _document, private http: HttpClient) {
    this.notifier = notifierService;
  };

  /*Change event of image file*/
  onFileChange(event) {
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];
    }
  }

  visibiltyedit() {
    this.visibilty = true;
  }

  selectChangeHandler(event: any) {
    //update the ui
    this.savedForm.controls.status.setValue(event.target.value);
    this.statusResult = event.target.value == 'Publish';
  }

  /*Submit a addnewblog form*/
  onSubmit(data) {
    if (this.savedForm.invalid) {
      this.submitted = true;
    } 
    else 
    {
      let html = tinymce.get('full-featured').getContent()
      var truevalues = this.savedForm.value.categorie.filter(function(element) {
        return (element == true);
      })
    
      if(truevalues == '') {
        this.notifier.notify('error','Category is required');
      } 
      else if(this.tags.length == 0) {
        this.notifier.notify('error','Tag is required');
      } 
      else if(html.indexOf('<p>') == -1) {
        this.notifier.notify('error','Blog detail is required');
      }
      else 
      {
        this.spinner.show();
        this.savedForm.controls['html'].setValue(html);
        this.savedForm.controls['tag'].setValue(this.tags.join(','));
        let now = new Date();
        if (this.savedForm.value.publish_on == 'Immediately') {
          this.savedForm.controls['publish_date'].setValue(now);
        }
        let formData = new FormData()
        for (let d in this.savedForm.value) {
          formData.append(d, this.savedForm.value[d])
        }
        let d = [];
        this.data.forEach((element, index) => {
          if (this.savedForm.value.categorie[index]) {
            d.push(element.id)
          }
        });

        formData.append('categories', d.join(','))
        formData.append('file', this.file);
        /* Add new blog */
        this.http.post(AppSettings.API_ENDPOINT + '/addblog', formData).subscribe((response: any) => {
          this.spinner.hide();
          if (response.status == 200) {
            $("#message-modal").modal("hide");
            this.router.navigate(['/bloglist'])
            this.notifier.notify('success', 'Added successfully');
          }
          else {
            this.notifier.notify('error', response.message);
          } 
        });
      }
    }
  };

  /** add taged to taged chip  ***/
  taged(val) {
    if(val != '') {
      this.tag = val;
      this.tags.push(this.tag) 
    } 
  }
  
  /* Get a categories  */
  getCategoryList() {
    this.http.get(AppSettings.API_ENDPOINT + 'categorylist').subscribe((res: any) => {
      if (res.status == true) {
        let arra = new FormArray([]);
        res.data.forEach(element => {
          arra.push(new FormControl(false))
        });
        this.savedForm.setControl('categorie', arra)
        this.data = res.data;
      }
    }) 
  }

  ngOnInit() {
    this.getCategoryList();
    
    /* Render a blog page  */
    setTimeout(() => {
      tinymce.init({
        selector: 'textarea#full-featured',
        plugins: 'paste print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount tinymcespellchecker a11ychecker imagetools textpattern help formatpainter permanentpen pageembed tinycomments mentions linkchecker',
        toolbar: 'formatselect | bold italic strikethrough forecolor backcolor permanentpen formatpainter | link image media pageembed | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat | addcomment',
        image_advtab: true,
        content_css: [
          '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
          '//www.tiny.cloud/css/codepen.min.css'
        ],
        link_list: [
          { title: 'My page 1', value: 'http://www.tinymce.com' },
          { title: 'My page 2', value: 'http://www.moxiecode.com' }
        ],
        image_list: [
          { title: 'My page 1', value: 'http://www.tinymce.com' },
          { title: 'My page 2', value: 'http://www.moxiecode.com' }
        ],
        image_class_list: [
          { title: 'None', value: '' },
          { title: 'Some class', value: 'class-name' }
        ],
        importcss_append: true,
        height: 400,
        file_picker_callback: function (callback, value, meta) {
          /* Provide file and text for the link dialog */
          if (meta.filetype === 'file') {
            callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
          }

          /* Provide image and alt text for the image dialog */
          if (meta.filetype === 'image') {
            callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
          }

          /* Provide alternative source and posted for the media dialog */
          if (meta.filetype === 'media') {
            callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
          }
        },
        templates: [
          { title: 'Some title 1', description: 'Some desc 1', content: 'My content' },
          { title: 'Some title 2', description: 'Some desc 2', content: '<div class="mceTmpl"><span class="cdate">cdate</span><span class="mdate">mdate</span>My content2</div>' }
        ],
        template_cdate_format: '[CDATE: %m/%d/%Y : %H:%M:%S]',
        template_mdate_format: '[MDATE: %m/%d/%Y : %H:%M:%S]',
        image_caption: true,
        spellchecker_dialog: true,
        spellchecker_whitelist: ['Ephox', 'Moxiecode'],
        tinycomments_mode: 'embedded',
        content_style: '.mce-annotation { background: #fff0b7; } .tc-active-annotation {background: #ffe168; color: black; }'
      });
    }, 500);
    try{
      tinymce.remove('textarea#full-featured');
    }catch(e){
     
    }
  };
}
