import { Component, OnInit, Inject, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { AppSettings } from '../app-setting';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
declare var $: any;
import { HttpClient, } from '@angular/common/http';
declare var Vvveb: any;

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-footerlist',
  templateUrl: './footerlist.component.html',
  styleUrls: ['./footerlist.component.scss']
})
export class FooterlistComponent implements OnInit {
  public cloneForm: FormGroup = new FormGroup({
    pagename: new FormControl('', Validators.required),
    title: new FormControl('', Validators.required),
    subtitle: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    bgimage: new FormControl('', Validators.required),
    metatitle: new FormControl('', Validators.required),
    metadescription: new FormControl('', Validators.required),
    html: new FormControl('')
  });

  path: any = AppSettings.IMAGE_PATH;
  dtOptions: any;
  data: any = [];
  page: any = [];
  file: any;
  id: any;
  public searchTxt: any;
  selectedvalue: string = 'home';
  private readonly notifier: NotifierService;

  constructor(private notifierService: NotifierService, private spinner: NgxSpinnerService, private http: HttpClient, private _renderer2: Renderer2, @Inject(DOCUMENT) private _document, private router: Router) { 
    this.notifier = notifierService;
  }
  
  ngOnInit() {
    this.getFooterList();
    $(document).on('click', '.table-edit', function (res, data) {
      this.data = {
        ajax: {
          url: AppSettings.API_ENDPOINT + "/footerlist",
          type: 'GET',
          success: function (result) { },
          data: function (data) {
            return JSON.stringify(data);
          }
        },
      }
    });
  }

  /*Get a footer listing*/
  getFooterList()
  {
    this.http.get(AppSettings.API_ENDPOINT + "footerlist").subscribe((res: any) => {
      if (res.status == 200) {
        this.spinner.hide()
        this.data = res.data;
      } else {
        this.notifier.notify('error', 'Something went wrong');
      }
    })
  }

  /*Go to footer for add new footer*/
  newpage() {
    this.router.navigate(['footer']);
  }

  //event handler for the select element's change event
  selectChangeHandler(event: any) {
    //update the ui
    this.selectedvalue = event.target.value;
  }

  /* event handler for the image file change event*/
  onFileChange(event) {
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];

    }
  }

  /*Submit addnew footer form */
  onSubmit(data) {
    this.spinner.show();
    let html = $('#message-modal textarea').val();
    this.cloneForm.controls['html'].setValue(html);
    let formData = new FormData()
    for (let d in this.cloneForm.value) {
      formData.append(d, this.cloneForm.value[d])
    }

    formData.append('file', this.file);
    /*Call api for add new footer*/
    return this.http.post(AppSettings.API_ENDPOINT + '/add', formData).subscribe((response: any) => {
      this.spinner.hide();
      if (response.status == 200) {
        $("#message-modal").modal("hide");
        this.router.navigate(['/'])
        this.notifier.notify('success', 'Added successfully');
      }
      else {
        this.notifier.notify('error', response.message);
      }
    });
  };

  /*Go to edit footer update footer*/
  tedit(val) {
    this.page = val;
    let pagename = btoa(val)
    this.router.navigate(['editfooter/' + pagename]);
  }

  /*Delete a footer*/
  footerdel() {
    this.http.post(AppSettings.API_ENDPOINT + "deletefooter", { "id": this.id }).subscribe((res: any) => {
      if (res.status == 200) {
        $('#delete-modal').modal('hide');
        this.getFooterList();
        this.notifier.notify('success', 'Deleted successfully');
      } else {
        this.notifier.notify('error', 'Something went wrong');
      }
    })
  }

  /*Open modal for  delete footer*/
  delete(id) {
    this.id = id,
    $('#delete-modal').modal('show');
  }
}
