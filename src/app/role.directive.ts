import { Directive ,Input,Renderer2,ElementRef} from '@angular/core';

@Directive({
  selector: '[appRole],[menuId],[permId]'
})
export class RoleDirective {
  @Input('appRole')  per:any;
  @Input('menuId') menuid:any;
  @Input('permId') permid:any;
  constructor(private re:Renderer2,private element:ElementRef) {
    
   }
   ngAfterViewInit(){
     
    let data = this.per.filter((d)=>{
      return d.menu_id == this.menuid && d.prem_id == this.permid
    
    })
    
    if(data.length != 0){
      this.element.nativeElement.checked= true;
    }
   }
}
