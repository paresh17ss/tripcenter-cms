
module.exports = (sequelize,DataTypes) =>{
	const footer = sequelize.define('footer',{
		id:{
			allowNull:false,
			autoIncrement:true,
			primaryKey:true,
			type:DataTypes.INTEGER
		},
		footer_name:DataTypes.STRING,
		url:DataTypes.STRING,
        isDeleted:{type:DataTypes.INTEGER, defaultValue: 0}	
       
		
		
	},{
		timestamps:false
	});

	footer.associate = function(models){

	};
	return footer;
}