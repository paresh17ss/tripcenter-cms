
module.exports = (sequelize,DataTypes) =>{
	const permission = sequelize.define('permission',{
		id:{
			allowNull:false,
			autoIncrement:true,
			primaryKey:true,
			type:DataTypes.INTEGER
		},
		name:DataTypes.TEXT,
		menu_id:DataTypes.INTEGER,
		
		
	},{
		timestamps:false
	});

	permission.associate = function(models){

	};
	return permission;
}