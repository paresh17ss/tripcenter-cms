
module.exports = (sequelize,DataTypes) =>{
	const news_category_mapping = sequelize.define('news_category_mapping',{
		id:{
			allowNull:false,
			autoIncrement:true,
			primaryKey:true,
			type:DataTypes.INTEGER
		},
        news_id:DataTypes.INTEGER,
        category_id:DataTypes.INTEGER
	},{
		timestamps:true
	});

	news_category_mapping.associate = function(models){
		
	};
	return news_category_mapping;
}

