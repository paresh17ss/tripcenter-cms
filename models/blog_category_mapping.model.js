
module.exports = (sequelize,DataTypes) =>{
	const blog_category_mapping = sequelize.define('blog_category_mapping',{
		id:{
			allowNull:false,
			autoIncrement:true,
			primaryKey:true,
			type:DataTypes.INTEGER
		},
        blog_id:DataTypes.INTEGER,
        category_id:DataTypes.INTEGER
		
		
	},{
		timestamps:true
	});

	blog_category_mapping.associate = function(models){
		
	};
	return blog_category_mapping;
}

