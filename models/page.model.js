
module.exports = (sequelize,DataTypes) =>{
	const Page = sequelize.define('page',{
		id:{
			allowNull:false,
			autoIncrement:true,
			primaryKey:true,
			type:DataTypes.INTEGER
		},
		pagename:DataTypes.STRING,
		title: DataTypes.TEXT,
		subtitle:DataTypes.TEXT,
		description:DataTypes.TEXT,
		template: DataTypes.TEXT,
		url:DataTypes.STRING,
		metatitle:DataTypes.TEXT,
		metadescription:DataTypes.TEXT,
		meta_keywords:DataTypes.STRING,
		bgimage:DataTypes.TEXT,
		// footer_id:DataTypes.INTEGER,
		publish_html:DataTypes.TEXT,
		draft_html:DataTypes.TEXT,
		isDeleted:{type:DataTypes.INTEGER, defaultValue: 0}	
	},{
		timestamps:false
	});

	Page.associate = function(models){
		//models.page.belongsTo(models.footer,  {foreignKey:  'footer_id', targetKey:'id'  });
	};
	return Page;
}