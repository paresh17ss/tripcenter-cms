
module.exports = (sequelize,DataTypes) =>{
	const contactUs = sequelize.define('contactUs',{
		id:{
			allowNull:false,
			autoIncrement:true,
			primaryKey:true,
			type:DataTypes.INTEGER
		},
		firstName:DataTypes.STRING,
		lastName:DataTypes.STRING,
		email:DataTypes.STRING,
		phoneNo:DataTypes.STRING,
		subject:DataTypes.STRING,
		interestedIn:DataTypes.STRING,
		booking:DataTypes.STRING,
		message:DataTypes.STRING,
		isDeleted:{type:DataTypes.INTEGER, defaultValue: 0}	
	},{
		timestamps:true
	});

	contactUs.associate = function(models){
		
	};
	return contactUs;
}

