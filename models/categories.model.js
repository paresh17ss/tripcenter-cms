
module.exports = (sequelize,DataTypes) =>{
	const categories = sequelize.define('categories',{
		id:{
			allowNull:false,
			autoIncrement:true,
			primaryKey:true,
			type:DataTypes.INTEGER
		},
		name:DataTypes.STRING,
		url:DataTypes.STRING,
        isDeleted:{type:DataTypes.INTEGER, defaultValue: 0}	
	},{
		timestamps:true
	});

	categories.associate = function(models){
	};
	return categories;
}

