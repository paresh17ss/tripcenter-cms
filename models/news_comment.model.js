module.exports = (sequelize, DataTypes) => {
  const news_comment = sequelize.define(
    "news_comment",
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      news_id: DataTypes.INTEGER,
      name: DataTypes.TEXT,
      message: DataTypes.TEXT,
      mail: DataTypes.TEXT,
      website: DataTypes.TEXT,
      status: { type: DataTypes.INTEGER, defaultValue: 0 },
      user_id: { type: DataTypes.INTEGER, defaultValue: 0 },
      isDeleted: { type: DataTypes.INTEGER, defaultValue: 0 },
      isReply: { type: DataTypes.BOOLEAN, defaultValue: false },
      belong_to_comment: { type: DataTypes.INTEGER, defaultValue: 0 }
    },
    {
      timestamps: true
    }
  );

  news_comment.associate = function(models) {};
  return news_comment;
};
