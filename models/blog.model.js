
module.exports = (sequelize,DataTypes) =>{
	const blog = sequelize.define('blog',{
		id:{
			allowNull:false,
			autoIncrement:true,
			primaryKey:true,
			type:DataTypes.INTEGER
		},
		postname:DataTypes.TEXT,
		url:DataTypes.STRING,
		drafthtml:DataTypes.TEXT,
		publishhtml:DataTypes.TEXT,
		meta_title:DataTypes.STRING,
		meta_description:DataTypes.STRING,
		status:DataTypes.STRING,
		visibility:DataTypes.STRING,
		publish_on:DataTypes.STRING,
		publish_date:DataTypes.DATEONLY,
		tag:DataTypes.TEXT,
        categories:DataTypes.STRING,
		feature_image:DataTypes.TEXT,
        user_id:DataTypes.INTEGER,
		isDeleted:{type:DataTypes.INTEGER, defaultValue: 0}           
	},{
		timestamps:true
	});

	blog.associate = function(models){
	};
	return blog;
}

