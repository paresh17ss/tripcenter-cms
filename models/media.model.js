
module.exports = (sequelize,DataTypes) =>{
	const media = sequelize.define('media',{
		id:{
			allowNull:false,
			autoIncrement:true,
			primaryKey:true,
			type:DataTypes.INTEGER
		},
		title:DataTypes.STRING,
		image:DataTypes.TEXT,
		isDeleted:{type:DataTypes.INTEGER, defaultValue: 0}	
		
		
	},{
		timestamps:true
	});

	media.associate = function(models){
	
	};
	return media;
}

