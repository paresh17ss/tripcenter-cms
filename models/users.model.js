
module.exports = (sequelize,DataTypes) =>{
	const users = sequelize.define('users',{
		id:{
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV1,
			primaryKey: true
		},
		email:DataTypes.STRING,
		password:DataTypes.STRING,
		role:{type:DataTypes.UUID, defaultValue: DataTypes.UUIDV1},
		dateCreated:DataTypes.DATEONLY,
		dateAcceptedTermsAndConditions:DataTypes.DATEONLY,
		isSuspend:{type:DataTypes.INTEGER, defaultValue: 0},
		menuList:DataTypes.STRING,
		manager:DataTypes.STRING,
		passwordReset:{type:DataTypes.INTEGER, defaultValue: 0},	
		isBToE:{type:DataTypes.INTEGER, defaultValue: 0},	
		passwordAutoReset:{type:DataTypes.INTEGER, defaultValue: 0},	
		jobTitle:DataTypes.STRING,
		telPhone:DataTypes.STRING,
		emergency:DataTypes.STRING,
		department:DataTypes.STRING,
		salutation:DataTypes.STRING,
		fName:DataTypes.STRING,
		lName:DataTypes.STRING,
		countryCode:DataTypes.STRING,
		currency:DataTypes.STRING,
		distance:DataTypes.STRING,

	},{
		timestamps:false
	});

	users.associate = function(models){
		models.users.belongsTo(models.roles,  {foreignKey: 'role', targetKey:'id', as:'roles'  });
	};
	return users;
}