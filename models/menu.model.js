
module.exports = (sequelize,DataTypes) =>{
	const menu = sequelize.define('menu',{
		id:{
			allowNull:false,
			autoIncrement:true,
			primaryKey:true,
			type:DataTypes.INTEGER
		},
		name:DataTypes.STRING,
		url:DataTypes.TEXT,
		
		
	},{
		timestamps:false
	});

	menu.associate = function(models){
		models.menu.hasMany(models.permission,  {foreignKey:  'menu_id', targetKey:'id'  });
	};
	return menu;
}

