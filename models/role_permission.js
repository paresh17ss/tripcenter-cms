
module.exports = (sequelize,DataTypes) =>{
	const role_permission = sequelize.define('role_permission',{
		id:{
			allowNull:false,
			autoIncrement:true,
			primaryKey:true,
			type:DataTypes.INTEGER
		},
		prem_id:DataTypes.INTEGER,
        menu_id:DataTypes.INTEGER,
        role_id:DataTypes.INTEGER,
		
		
	},{
		timestamps:false
	});

	role_permission.associate = function(models){

	};
	return role_permission;
}