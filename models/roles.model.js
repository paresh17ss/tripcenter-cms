
module.exports = (sequelize,DataTypes) =>{
	const roles = sequelize.define('roles',{
		id:{
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV1,
			primaryKey: true
		},
		key:DataTypes.STRING,
		displayValue:DataTypes.STRING,
		isSuperUser:{type:DataTypes.INTEGER, defaultValue: 0},
	},
	{
		timestamps:false
	});

	roles.associate = function(models){
	};
	return roles;
}

