
module.exports = (sequelize,DataTypes) =>{
	const role_master = sequelize.define('role_master',{
		id:{
			allowNull:false,
			autoIncrement:true,
			primaryKey:true,
			type:DataTypes.INTEGER
		},
		name:DataTypes.TEXT,
		is_deleted:{type:DataTypes.INTEGER, defaultValue: 0},
		
		
	},{
		timestamps:false
	});

	role_master.associate = function(models){

	};
	return role_master;
}