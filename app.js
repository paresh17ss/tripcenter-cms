const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const path = require("path");
const models = require("./models");
const fileUpload = require("express-fileupload");

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
app.use(fileUpload());
app.use(
  bodyParser.urlencoded({ urlencoded: false, limit: "10000mb", extended: true })
);
app.use(bodyParser.json({ limit: "10000mb" }));
app.use(express.static(path.join(__dirname, "src/assets")));
require("./routes/page.route")(app);
require("./routes/menu.route")(app);
require("./routes/blog.route")(app);
require("./routes/blog_comment.route")(app);
require("./routes/categories.route")(app);
require("./routes/media.route")(app);
require("./routes/users.route")(app);
require("./routes/news.route")(app);
require("./routes/footer.route")(app);
require("./routes/news_comment.route")(app);
require("./routes/contact_us.route")(app);
require("./routes/supplier_demo.route")(app);
require("./routes/roles.route")(app);

models.sequelize.sync().then(() => {
  app.listen(3000, () => {
    console.log("Server connected");
  });
});
